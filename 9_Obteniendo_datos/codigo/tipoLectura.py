# 'r' significa solo lectura
file_for_reading = open('reading_file.txt', 'r')
# 'w' es escritura: ¡destruirá el archivo si ya existe!
file_for_writing = open('writing_file.txt', 'w')
# 'a' está adjunto, para agregar al final del archivo
file_for_appending = open('appending_file.txt', 'a')
# no olvides cerrar tus archivos cuando hayas terminado
file_for_writing.close()
