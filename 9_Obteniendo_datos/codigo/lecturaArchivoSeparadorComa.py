import csv
with open('tab_delimited_stock_prices.txt', 'rt', encoding='utf8') as f:
    reader = csv.reader(f, delimiter=',')
    for row in reader:
        print(row[1])
        date = row[0]
        symbol = row[1]
        closing_price = float(row[2])
        print(date, symbol, closing_price)  