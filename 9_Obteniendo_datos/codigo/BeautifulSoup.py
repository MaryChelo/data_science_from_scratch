from bs4 import BeautifulSoup
import requests
html = requests.get("http://www.example.com").text
soup = BeautifulSoup(html, 'html5lib')
print(soup)
first_paragraph = soup.find('p') 
print(first_paragraph)
first_paragraph_text = soup.p.text
first_paragraph_words = soup.p.text.split()
spans_inside_divs = [span for div in soup('div') for span in div('span')] 