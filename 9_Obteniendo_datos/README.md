# Obteniendo los datos

Para ser un científico de datos necesitas datos. De hecho, como científico de datos, pasará una parte vergonzosamente grande de su tiempo adquiriendo, limpiando y transformando datos. En caso de apuro, siempre puedes escribir los datos en ti mismo (o si tienes minions, haz que lo hagan), pero generalmente esto no es un buen uso de tu tiempo. En este capítulo, veremos diferentes formas de obtener datos en Python y en los formatos correctos.

## Archivos

Si ejecuta sus scripts de Python en la línea de comandos, puede canalizar los datos a través de ellos utilizando sys.stdin y sys.stdout.

```python
# egrep.py
import sys, re
# sys.argv es la lista de argumentos de la línea de comandos
# sys.argv [0] es el nombre del programa en sí
# sys.argv [1] será la expresión regular especificada en la línea de comando
regex = sys.argv[1]
# para cada línea pasada en el script
for line in sys.stdin:
    # si coincide con la expresión regular, escríbala a la salida estándar
    if re.search(regex, line):
        sys.stdout.write(line)
```

Y aquí hay uno que cuenta las líneas que recibe y luego escribe el conteo:

```python
# line_count.py
import sys
count = 0
for line in sys.stdin:
    count += 1
    # la impresión va a sys.stdout
    print count
```

**Archivos de lectura**

También puede leer y escribir explícitamente en archivos directamente en su código.

* El primer paso para trabajar con un archivo de texto es obtener un objeto de archivo usando abierto:

```python
# 'r' significa solo lectura
file_for_reading = open('reading_file.txt', 'r')
```

```python
# 'w' es escritura: ¡destruirá el archivo si ya existe!
file_for_writing = open('writing_file.txt', 'w')
```

```python
# 'a' está adjunto, para agregar al final del archivo
file_for_appending = open('appending_file.txt', 'a')
```

```python
# no olvides cerrar tus archivos cuando hayas terminado
file_for_writing.close()
```

Debido a que es fácil olvidar cerrar sus archivos, siempre debe usarlos en un bloque with, al final de los cuales se cerrarán automáticamente:

```python
with open('reading_file.txt','r') as f:
    data = function_that_gets_data_from(f)
    # en este punto f ya se ha cerrado, así que no intentes usarlo
    process(data)
```

**Archivos delimitados**

Estructura del archivo

>tab_delimited_stock_prices.txt

```
6/20/2014,AAPL,90.91
6/20/2014,MSFT,41.68
6/20/2014,FB,64.5
6/19/2014,AAPL,91.86
6/19/2014,MSFT,41.51
6/19/2014,FB,64.34
```

Lectura de un archivo .txt donde el delimitador es ','

```python
import csv
with open('tab_delimited_stock_prices.txt', 'rt', encoding='utf8') as f:
    reader = csv.reader(f, delimiter=',')
    for row in reader:
        print(row[1])
        date = row[0]
        symbol = row[1]
        closing_price = float(row[2])
        print(date, symbol, closing_price)  
```

Estructura del archivo

>tab_delimited_stock_prices.txt

```
date:symbol:closing_price
6/20/2014:AAPL:90.91
6/20/2014:MSFT:41.68
6/20/2014:FB:64.5
```

```python
with open('colon_delimited_stock_prices.txt', 'r', encoding='utf8',newline='') as f:
        reader = csv.DictReader(f, delimiter=':')
        for row in reader:
            print(row)
            date = row["date"]
            symbol = row["symbol"]
            closing_price = float(row["closing_price"])
            print(date, symbol, closing_price)

```

**Escribiendo en un archivo"**

>comma_delimited_stock_prices.txt

```
FB,64.5
MSFT,41.68
AAPL,90.91
```

Datos a escribir

```
today_prices = { 'AAPL' : 90.91, 'MSFT' : 41.68, 'FB' : 64.5 }
```

```python
with open('comma_delimited_stock_prices.txt','w', encoding='utf8',newline='') as f:
    writer = csv.writer(f, delimiter=',')
    for stock, price in today_prices.items():
        writer.writerow([stock, price])
```

## Scraping the Web

Otra forma de obtener datos es eliminándolo de las páginas web. Resulta que recuperar páginas web es bastante fácil.
Web scraping es un término usado para describir el uso de un programa o algoritmo para extraer y procesar grandes cantidades de datos de la web. El raspado web con Python es una habilidad que puede usar para extraer los datos en una forma útil que se puede importar.

**HTML y su análisis**

El lenguaje de marcado de hipertexto es un lenguaje de marcado que le dice a un navegador cómo diseñar el contenido.
Las páginas en la Web están escritas en HTML, en el que el texto (idealmente) está marcado en elementos y sus atributos.

```html
<html>
	<head>
		<title>A web page</title>
	</head>
	<body>
		<p id="author">Joel Grus</p>
		<p id="subject">Data Science</p>
	</body>
</html>
```

Para obtener datos de HTML, usaremos la biblioteca <code>BeautifulSoup</code>, que construye un árbol a partir de los diversos elementos en una página web y proporciona una interfaz simple para acceder a ellos.

**Instalación con Anaconda**

Si se realiza la instalación desde Anaconda o minianaconda:

```bash
conda install -c anaconda beautifulsoup4 
```

**Instalación de Python**

```shell
$ python setup.py install
```

Se puede instalar usando <code> pip </code>, el nombre del paquete beautifulsoup4 que funciona para Python 2 y Python 3.

```shell
$ pip install beautifulsoup4
```

```shell
$ pip install lxml
$ pip install html5lib
```

Para usar <code>Beautiful Soup</code>, necesitaremos pasar algo de HTML a la función BeautifulSoup (). En nuestros ejemplos, esto será el resultado de una llamada a <code>requests.get</code>

```python
from bs4 import BeautifulSoup
import requests
html = requests.get("http://www.example.com").text
soup = BeautifulSoup(html, 'html5lib')
```

Después de lo cual podemos llegar bastante lejos usando algunos métodos simples. Normalmente trabajaremos con objetos de etiqueta, que corresponden a las etiquetas que representan la estructura de una página HTML. Por ejemplo, para encontrar la primera etiqueta (y su contenido) puede usar:

```python
first_paragraph = soup.find('p') 
```

```<p>This domain is established to be used for illustrative examples in documents.</p>```

Puede obtener el contenido de texto de una etiqueta utilizando su propiedad de texto:

```python
first_paragraph_text = soup.p.text
first_paragraph_words = soup.p.text.split()
```

```['This',
 'domain',
 'is',
 'established',
 'to',
 'be',
 'used',
 'for',
 'illustrative',
 'examples',
 'in',
 'documents.',
 'You',
 'may',
 'use',
 'this',
 'domain',
 'in',
 'examples',
 'without',
 'prior',
 'coordination',
 'or',
 'asking',
 'for',
 'permission.']```

Y puede extraer los atributos de una etiqueta tratándola como un dict:

```python
first_paragraph_id = soup.p['id'] 
first_paragraph_id2 = soup.p.get('id') 
```

Puede obtener varias etiquetas a la vez:

```python
all_paragraphs = soup.find_all('p')
paragraphs_with_ids = [p for p in soup('p') if p.get('id')]
```

Y puedes combinarlos para implementar una lógica más elaborada. Por ejemplo, si desea buscar cada elemento <code>span</code> que está contenido dentro de un elemento <code>div</code>, puede hacer esto:

```python
# advertencia, devolverá el mismo intervalo varias veces
# si se encuentra dentro de múltiples divs
spans_inside_divs = [span
for div in soup('div')# para cada <div> en la página
for span in div('span')]  # encuentra cada <span> dentro de él
```

## Ejemplo: O'Reilly Books About Data 

Un posible inversor en DataSciencester cree que los datos son solo una moda pasajera. Para demostrar que está equivocado, decide examinar cuántos libros de datos ha publicado O’Reilly a lo largo del tiempo. Después de buscar en su sitio web, descubre que tiene muchas páginas de libros de datos (y videos), accesibles a través de páginas de directorio de 30 elementos a la vez con direcciones URL como:

```https://www.safaribooksonline.com/search/?query=data```

Para descubrir cómo extraer los datos, descarguemos una de esas páginas y la enviemos a Beautiful Soup:

```python
from bs4 import BeautifulSoup
import requests

#usted no tiene que dividir la url de esta manera a menos que tenga que caber en un libro
url = "http://shop.oreilly.com/category/browse-subjects/" + \
"data.do?sortby=publicationDate&page=1"
soup = BeautifulSoup(requests.get(url).text, 'html5lib')
```

```html
<td class="thumbtext">
	<div class="thumbcontainer">
		<div class="thumbdiv">
			<a href="/product/9781118903407.do">
				<img src="..."/>
			</a>
		</div>
	</div>
	<div class="widthchange">
		<div class="thumbheader">
			<a href="/product/9781118903407.do">Getting a Big Data Job For Dummies</a>
		</div>
		<div class="AuthorName">By Jason Williamson</div>
			<span class="directorydate"> December 2014 </span>
			<div style="clear:both;">
			<div id="146350">
				<span class="pricelabel">
			Ebook:
			<span class="price">&nbsp;$29.99</span>
			</span>
			</div>
		</div>
	</div>
</td>
```

Extrayendo los datos:

```python
from collections import Counter
import math, random, csv, json, re

from bs4 import BeautifulSoup
import requests
def is_video(td):
    """es un video si tiene exactamente una etiqueta de precio, y si
    el texto eliminado dentro de esa etiqueta de precio comienza con Video'"""
    pricelabels = td('span', 'pricelabel')
    return (len(pricelabels) == 1 and
            pricelabels[0].text.strip().startswith("Video"))

def book_info(td):
    """recibió una etiqueta BeautifulSoup <td> que representa un libro,
    extraer los detalles del libro y devolver un dict"""

    title = td.find("div", "thumbheader").a.text
    by_author = td.find('div', 'AuthorName').text
    authors = [x.strip() for x in re.sub("^By ", "", by_author).split(",")]
    isbn_link = td.find("div", "thumbheader").a.get("href")
    isbn = re.match("/product/(.*)\.do", isbn_link).groups()[0]
    date = td.find("span", "directorydate").text.strip()

    return {
        "title" : title,
        "authors" : authors,
        "isbn" : isbn,
        "date" : date
    }

from time import sleep

def scrape(num_pages=31):
    base_url = "http://shop.oreilly.com/category/browse-subjects/" + \
           "data.do?sortby=publicationDate&page="

    books = []

    for page_num in range(1, num_pages + 1):
        print("souping page", page_num)
        url = base_url + str(page_num)
        soup = BeautifulSoup(requests.get(url).text, 'html5lib')

        for td in soup('td', 'thumbtext'):
            if not is_video(td):
                books.append(book_info(td))

        # n¡Ahora sé un buen ciudadano y respeta el robots.txt!
        sleep(30)

    return books

def get_year(book):
    """book [" date "] se parece a 'Noviembre de 2014', por lo que necesitamos
    dividir en el espacio y luego tomar la segunda pieza"""
    return int(book["date"].split()[1])

```

