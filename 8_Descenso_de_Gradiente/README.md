# Descenso de gradiente

## La idea detrás de la pendiente de gradiente

Con frecuencia al hacer ciencia de datos, trataremos de encontrar el mejor modelo para una situación determinada. Y generalmente, "mejor" significará algo como "minimiza el error del modelo" o "maximiza la probabilidad de los datos". 

<br/>

Esto significa que tendremos que resolver una serie de problemas de optimización. Y, en particular, tendremos que resolverlos desde cero. Nuestro enfoque será una técnica llamada descenso de gradiente.

Supongamos que tenemos alguna función f que toma como entrada un vector de números reales y genera un solo número real. Una simple función de este tipo es:

```python
def sum_of_squares(v):
    """calcula la suma de los elementos cuadrados en v"""
    return sum(v_i ** 2 for v_i in v)
```

Con frecuencia necesitaremos maximizar (o minimizar) tales funciones. Es decir, necesitamos encontrar la entrada v que produce el valor más grande (o más pequeño) posible.

* Para funciones como la nuestra, el gradiente (si recuerda su cálculo, este es el vector de derivadas parciales) proporciona la dirección de entrada en la que la función aumenta más rápidamente.

![](imagenes/1_descensoGradiente.png)

Si f es una función de una variable, su derivada en un punto x mide cómo f (x) cambia cuando realizamos un cambio muy pequeño en x. Se define como el límite de los cocientes de diferencia:

```python
def difference_quotient(f, x, h):
    return (f(x + h) - f(x)) / h
```

Para estimar el gradiente

```python
def estimate_gradient(f, v, h=0.00001):
    return [partial_difference_quotient(f, v, i, h)
            for i, _ in enumerate(v)]
```

```python
def sum_of_squares_gradient(v):
    return [2 * v_i for v_i in v]
```

Minimizar / maximizar lote

```python
def minimize_batch(target_fn, gradient_fn, theta_0, tolerance=0.000001):
    """use el gradiente de descenso para encontrar theta que minimice la función de destino"""

    step_sizes = [100, 10, 1, 0.1, 0.01, 0.001, 0.0001, 0.00001]
    theta = theta_0 # establece theta al valor inicial
	target_fn = safe (target_fn) # versión segura de target_fn
	value = target_fn (theta) # value estamos minimizando

    while True:
        gradient = gradient_fn(theta)
        next_thetas = [step(theta, gradient, -step_size)
                       for step_size in step_sizes]

        # Elige la que minimice la función de error.
        next_theta = min(next_thetas, key=target_fn)
        next_value = target_fn(next_theta)

        # detente si estamos "convergiendo"
        if abs(value - next_value) < tolerance:
            return theta
        else:
            theta, value = next_theta, next_value

def negate(f):
    """devuelve una función que para cualquier entrada x devuelve -f (x)"""
    return lambda *args, **kwargs: -f(*args, **kwargs)

def negate_all(f):
    """lo mismo cuando f devuelve una lista de números"""
    return lambda *args, **kwargs: [-y for y in f(*args, **kwargs)]

def maximize_batch(target_fn, gradient_fn, theta_0, tolerance=0.000001):
    return minimize_batch(negate(target_fn),
                          negate_all(gradient_fn),
                          theta_0,
                          tolerance)
```

Minimizar / maximizar el estocástico

```python
def minimize_stochastic(target_fn, gradient_fn, x, y, theta_0, alpha_0=0.01):

    data = list(zip(x, y))
    theta = theta_0                             #  conjetura inicial
    alpha = alpha_0                             # tamaño de paso inicial
    min_theta, min_value = None, float("inf")   # tamaño de paso inicial
    iterations_with_no_improvement = 0

    #  si alguna vez hacemos 100 iteraciones sin mejoras, deténgase
    while iterations_with_no_improvement < 100:
        value = sum( target_fn(x_i, y_i, theta) for x_i, y_i in data )

        if value < min_value:
        	# Si hemos encontrado un nuevo mínimo, recuérdalo.
        	# y volver al tamaño de paso original
            min_theta, min_value = theta, value
            iterations_with_no_improvement = 0
            alpha = alpha_0
        else:
            # de lo contrario no estamos mejorando, así que intenta reducir el tamaño del paso
            iterations_with_no_improvement += 1
            alpha *= 0.9

        # y dar un paso de gradiente para cada uno de los puntos de datos
        for x_i, y_i in in_random_order(data):
            gradient_i = gradient_fn(x_i, y_i, theta)
            theta = vector_subtract(theta, scalar_multiply(alpha, gradient_i))

    return min_theta

def maximize_stochastic(target_fn, gradient_fn, x, y, theta_0, alpha_0=0.01):
    return minimize_stochastic(negate(target_fn),
                               negate_all(gradient_fn),
                               x, y, theta_0, alpha_0)
```

**Calcular Gradiente**

```python
if __name__ == "__main__":

    print("using the gradient")

    v = [random.randint(-10,10) for i in range(3)]

    tolerance = 0.0000001

    while True:
        #print v, sum_of_squares (v)
        gradient = sum_of_squares_gradient(v)   # calcula el gradiente en v
        next_v = step(v, gradient, -0.01)       # da un paso de gradiente negativo
        if distance(next_v, v) < tolerance:     # detener si estamos convergiendo
            break
        v = next_v                              # continuar si no estamos

    print("minimum v", v)
    print("minimum value", sum_of_squares(v))
    print()


    print("using minimize_batch")

    v = [random.randint(-10,10) for i in range(3)]

    v = minimize_batch(sum_of_squares, sum_of_squares_gradient, v)

    print("minimum v", v)
    print("minimum value", sum_of_squares(v))
```

``` 
Resultado:
using the gradient
minimum v [-1.0875470134613462e-06, -2.1750940269226923e-06, 4.350188053845385e-06]
minimum value 2.4837928636262565e-11

using minimize_batch
minimum v [0.0004984604984193437, 0.0008307674973655728, -0.0009969209968386874]
minimum value 1.93248897710136e-06
```