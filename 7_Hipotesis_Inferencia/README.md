# Hipótesis e Inferencia

##  Pruebas de hipótesis estadísticas 

**¿Qué haremos con todas estas estadísticas y la teoría de la probabilidad?** La parte científica de la ciencia de datos a menudo implica formular y probar hipótesis sobre nuestros datos y los procesos que los generan.

 En la configuración clásica, tenemos una hipótesis nula que representa una posición predeterminada y una hipótesis alternativa con la que nos gustaría compararla. Utilizamos las estadísticas para decidir si podemos rechazar como falsas o no. 

 **Ejemplo lanzar una moneda**

Imagina que tenemos una moneda y queremos probar si es justo. Supondremos que la moneda tiene cierta probabilidad p de cabezas de aterrizaje, por lo que nuestra hipótesis nula es que la moneda es justa, es decir, p = 0.5. Probaremos esto contra la hipótesis alternativa P no es igual a 5.
En particular, nuestra prueba incluirá lanzar la moneda un número n veces y contar el número de caras X.

```python
def normal_approximation_to_binomial(n, p):
    """encuentra mu y sigma correspondientes a un Binomio (n, p)"""
    mu = p * n
    sigma = math.sqrt(p * (1 - p) * n)
    return mu, sigma 
```


Siempre que una variable aleatoria siga una distribución normal, podemos usar normal_cdf para calcular la probabilidad de que su valor realizado se encuentre dentro (o fuera) de un intervalo particular:

```python
def uniform_cdf(x):
    "devuelve la probabilidad de que una variable aleatoria uniforme sea menor que x"
    if x < 0:   return 0    # uniform random nunca es menor que 0
    elif x < 1: return x    # e.g. P (X <0.4) = 0.4
    else:       return 1    # uniform random siempre es menor que 1
```

```python
def normal_pdf(x, mu=0, sigma=1):
    sqrt_two_pi = math.sqrt(2 * math.pi)
    return (math.exp(-(x-mu) ** 2 / 2 / sigma ** 2) / (sqrt_two_pi * sigma))
```

```python
def normal_cdf(x, mu=0,sigma=1):
    return (1 + math.erf((x - mu) / math.sqrt(2) / sigma)) / 2
```

```python
# el cdf normal _ es_ la probabilidad de que la variable esté por debajo de un umbral
normal_probability_below = normal_cdf
# está por encima del umbral si no está por debajo del umbral
def normal_probability_above(lo, mu=0, sigma=1):
    return 1 - normal_cdf(lo, mu, sigma)
# está entre si es menos que hola, pero no menos que lo
def normal_probability_between(lo, hi, mu=0, sigma=1):
    return normal_cdf(hi, mu, sigma) - normal_cdf(lo, mu, sigma)
# está afuera si no está entre
def normal_probability_outside(lo, hi, mu=0, sigma=1):
    return 1 - normal_probability_between(lo, hi, mu, sigma)
```

**P-hacking**

Un procedimiento que rechaza erróneamente la hipótesis nula, solo el 5% del tiempo, por definición, el 5% del tiempo rechazará erróneamente la hipótesis nula:

```python
def run_experiment():
    """flip a fair coin 1000 times, True = heads, False = tails"""
    return [random.random() < 0.5 for _ in range(1000)]
def reject_fairness(experiment):
    """using the 5% significance levels"""
    num_heads = len([flip for flip in experiment if flip])
    return num_heads < 469 or num_heads > 531
    random.seed(0)
    experiments = [run_experiment() for _ in range(1000)]
    num_rejections = len([experiment
    for experiment in experiments
        if reject_fairness(experiment)])
    print (num_rejections) 
```


**Probando**

```python
def normal_approximation_to_binomial(n, p):
    """encuentra mu y sigma correspondientes a un Binomio (n, p)"""
    mu = p * n
    sigma = math.sqrt(p * (1 - p) * n)
    return mu, sigma

import math, random

def uniform_cdf(x):
    "devuelve la probabilidad de que una variable aleatoria uniforme sea menor que x"
    if x < 0:   return 0    # uniform random nunca es menor que 0
    elif x < 1: return x    # e.g. P (X <0.4) = 0.4
    else:       return 1    # uniform random siempre es menor que 1


def normal_pdf(x, mu=0, sigma=1):
    sqrt_two_pi = math.sqrt(2 * math.pi)
    return (math.exp(-(x-mu) ** 2 / 2 / sigma ** 2) / (sqrt_two_pi * sigma))

def normal_cdf(x, mu=0,sigma=1):
    return (1 + math.erf((x - mu) / math.sqrt(2) / sigma)) / 2


# el cdf normal _ es_ la probabilidad de que la variable esté por debajo de un umbral
normal_probability_below = normal_cdf
# está por encima del umbral si no está por debajo del umbral
def normal_probability_above(lo, mu=0, sigma=1):
    return 1 - normal_cdf(lo, mu, sigma)
# está entre si es menos que hola, pero no menos que lo
def normal_probability_between(lo, hi, mu=0, sigma=1):
    return normal_cdf(hi, mu, sigma) - normal_cdf(lo, mu, sigma)
# está afuera si no está entre
def normal_probability_outside(lo, hi, mu=0, sigma=1):
    return 1 - normal_probability_between(lo, hi, mu, sigma)


def normal_approximation_to_binomial(n, p):
    """encuentra mu y sigma correspondientes a un binomio (n, p)"""
    mu = p * n
    sigma = math.sqrt(p * (1 - p) * n)
    return mu, sigma

def normal_two_sided_bounds(probability, mu=0, sigma=1):
    """devuelve los límites simétricos (sobre la media)
     que contienen la probabilidad especificada"""
    tail_probability = (1 - probability) / 2

    # límite superior debe tener tail_probability encima de él
    upper_bound = normal_lower_bound(tail_probability, mu, sigma)

    # el límite inferior debe tener la capacidad de cola debajo de él
    lower_bound = normal_upper_bound(tail_probability, mu, sigma)

    return lower_bound, upper_bound


def normal_upper_bound(probability, mu=0, sigma=1):
    """returns the z for which P(Z <= z) = probability"""
    return inverse_normal_cdf(probability, mu, sigma)

def normal_lower_bound(probability, mu=0, sigma=1):
    """devuelve la z para la cual P (Z <= z) = probabilidad"""
    return inverse_normal_cdf(1 - probability, mu, sigma)

def inverse_normal_cdf(p, mu=0, sigma=1, tolerance=0.00001):
    """Encontrar inverso aproximado utilizando la búsqueda binaria"""

    # if not standard, compute standard and rescale
    if mu != 0 or sigma != 1:
        return mu + sigma * inverse_normal_cdf(p, tolerance=tolerance)

    low_z, low_p = -10.0, 0            # normal_cdf (-10) es (muy cerca de) 0
    hi_z,  hi_p  =  10.0, 1            # normal_cdf (10) es (muy cerca de) 1
    while hi_z - low_z > tolerance:
        mid_z = (low_z + hi_z) / 2     # considera el punto medio
        mid_p = normal_cdf(mid_z)      # y el valor del cdf allí
        if mid_p < p:
            # el punto medio sigue siendo demasiado bajo, busque por encima de él
            low_z, low_p = mid_z, mid_p
        elif mid_p > p:
            # el punto medio sigue siendo demasiado alto, busque debajo
            hi_z, hi_p = mid_z, mid_p
        else:
            break

    return mid_z


def two_sided_p_value(x, mu=0, sigma=1):
    if x >= mu:
        # si x es mayor que la media, la cola está por encima de x
        return 2 * normal_probability_above(x, mu, sigma)
    else:
        # si x es menor que la media, la cola está por debajo de x
        return 2 * normal_probability_below(x, mu, sigma)

upper_p_value = normal_probability_above
lower_p_value = normal_probability_below


def run_experiment():
    """voltear una moneda justa 1000 veces, Verdadero = caras, Falso = cruz"""
    return [random.random() < 0.5 for _ in range(1000)]

def reject_fairness(experiment):
    """utilizando los niveles de significación del 5%"""
    num_heads = len([flip for flip in experiment if flip])
    return num_heads < 469 or num_heads > 531

def a_b_test_statistic(N_A, n_A, N_B, n_B):
    p_A, sigma_A = estimated_parameters(N_A, n_A)
    p_B, sigma_B = estimated_parameters(N_B, n_B)
    return (p_B - p_A) / math.sqrt(sigma_A ** 2 + sigma_B ** 2)


def estimated_parameters(N, n):
    p = n / N
    sigma = math.sqrt(p * (1 - p) / N)
    return p, sigma

if __name__ == "__main__":

    mu_0, sigma_0 = normal_approximation_to_binomial(1000, 0.5)
    print("mu_0", mu_0)
    print("sigma_0", sigma_0)
    print("normal_two_sided_bounds(0.95, mu_0, sigma_0)", normal_two_sided_bounds(0.95, mu_0, sigma_0))
    print()
    print("power of a test")

    print("95% bounds based on assumption p is 0.5")

    lo, hi = normal_two_sided_bounds(0.95, mu_0, sigma_0)
    print("lo", lo)
    print("hi", hi)

    print("actual mu and sigma based on p = 0.55")
    mu_1, sigma_1 = normal_approximation_to_binomial(1000, 0.55)
    print("mu_1", mu_1)
    print("sigma_1", sigma_1)

    # a type 2 error means we fail to reject the null hypothesis
    # which will happen when X is still in our original interval
    type_2_probability = normal_probability_between(lo, hi, mu_1, sigma_1)
    power = 1 - type_2_probability # 0.887

    print("type 2 probability", type_2_probability)
    print("power", power)
    print

    print("one-sided test")
    hi = normal_upper_bound(0.95, mu_0, sigma_0)
    print("hi", hi) # is 526 (< 531, since we need more probability in the upper tail)
    type_2_probability = normal_probability_below(hi, mu_1, sigma_1)
    power = 1 - type_2_probability # = 0.936
    print("type 2 probability", type_2_probability)
    print("power", power)
    print()

    print("two_sided_p_value(529.5, mu_0, sigma_0)", two_sided_p_value(529.5, mu_0, sigma_0))

    print("two_sided_p_value(531.5, mu_0, sigma_0)", two_sided_p_value(531.5, mu_0, sigma_0))

    print("upper_p_value(525, mu_0, sigma_0)", upper_p_value(525, mu_0, sigma_0))
    print("upper_p_value(527, mu_0, sigma_0)", upper_p_value(527, mu_0, sigma_0))
    print()

    print("P-hacking")

    random.seed(0)
    experiments = [run_experiment() for _ in range(1000)]
    num_rejections = len([experiment
                          for experiment in experiments
                          if reject_fairness(experiment)])

    print(num_rejections, "rejections out of 1000")
    print()

    print("A/B testing")
    z = a_b_test_statistic(1000, 200, 1000, 180)
    print("a_b_test_statistic(1000, 200, 1000, 180)", z)
    print("p-value", two_sided_p_value(z))
    z = a_b_test_statistic(1000, 200, 1000, 150)
    print("a_b_test_statistic(1000, 200, 1000, 150)", z)
    print("p-value", two_sided_p_value(z))
```

Resultado:
```
mu_0 500.0
sigma_0 15.811388300841896
normal_two_sided_bounds(0.95, mu_0, sigma_0) (469.01026640487555, 530.9897335951244)

power of a test
95% bounds based on assumption p is 0.5
lo 469.01026640487555
hi 530.9897335951244
actual mu and sigma based on p = 0.55
mu_1 550.0
sigma_1 15.732132722552274
type 2 probability 0.11345199870463285
power 0.8865480012953671
one-sided test
hi 526.0073585242053
type 2 probability 0.06362051966928273
power 0.9363794803307173

two_sided_p_value(529.5, mu_0, sigma_0) 0.06207721579598857
two_sided_p_value(531.5, mu_0, sigma_0) 0.046345287837786575
upper_p_value(525, mu_0, sigma_0) 0.056923149003329065
upper_p_value(527, mu_0, sigma_0) 0.04385251499101195

P-hacking
46 rejections out of 1000

A/B testing
a_b_test_statistic(1000, 200, 1000, 180) -1.1403464899034472
p-value 0.254141976542236
a_b_test_statistic(1000, 200, 1000, 150) -2.948839123097944
p-value 0.003189699706216853
```