import math, random
def uniform_cdf(x):
    "devuelve la probabilidad de que una variable aleatoria uniforme sea menor que x"
    if x < 0:   return 0    # uniform random nunca es menor que 0
    elif x < 1: return x    # e.g. P (X <0.4) = 0.4
    else:       return 1    # uniform random siempre es menor que 1
def normal_pdf(x, mu=0, sigma=1):
    sqrt_two_pi = math.sqrt(2 * math.pi)
    return (math.exp(-(x-mu) ** 2 / 2 / sigma ** 2) / (sqrt_two_pi * sigma))
def normal_cdf(x, mu=0,sigma=1):
    return (1 + math.erf((x - mu) / math.sqrt(2) / sigma)) / 2   

# el cdf normal _ es_ la probabilidad de que la variable esté por debajo de un umbral
normal_probability_below = normal_cdf
# está por encima del umbral si no está por debajo del umbral
def normal_probability_above(lo, mu=0, sigma=1):
    return 1 - normal_cdf(lo, mu, sigma)
# está entre si es menos que hola, pero no menos que lo
def normal_probability_between(lo, hi, mu=0, sigma=1):
    return normal_cdf(hi, mu, sigma) - normal_cdf(lo, mu, sigma)
# está afuera si no está entre
def normal_probability_outside(lo, hi, mu=0, sigma=1):
    return 1 - normal_probability_between(lo, hi, mu, sigma)    