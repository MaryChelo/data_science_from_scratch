# Sistemas de recomendación

> Otro problema de datos común es producir recomendaciones de algún tipo. 

Netflix recomienda películas que te gustaría ver. Amazon recomienda los productos que desee comprar. Twitter recomienda a los usuarios que quieras seguir. En este capítulo, veremos varias formas de usar los datos para hacer recomendaciones. 

En particular, veremos el conjunto de datos de usuarios_intereses que hemos usado antes:

```python
users_interests = [
    ["Hadoop", "Big Data", "HBase", "Java", "Spark", "Storm", "Cassandra"],
    ["NoSQL", "MongoDB", "Cassandra", "HBase", "Postgres"],
    ["Python", "scikit-learn", "scipy", "numpy", "statsmodels", "pandas"],
    ["R", "Python", "statistics", "regression", "probability"],
    ["machine learning", "regression", "decision trees", "libsvm"],
    ["Python", "R", "Java", "C++", "Haskell", "programming languages"],
    ["statistics", "probability", "mathematics", "theory"],
    ["machine learning", "scikit-learn", "Mahout", "neural networks"],
    ["neural networks", "deep learning", "Big Data", "artificial intelligence"],
    ["Hadoop", "Java", "MapReduce", "Big Data"],
    ["statistics", "R", "statsmodels"],
    ["C++", "deep learning", "artificial intelligence", "probability"],
    ["pandas", "R", "Python"],
    ["databases", "HBase", "Postgres", "MySQL", "MongoDB"],
    ["libsvm", "regression", "support vector machines"]
]
```

Y pensaremos en el problema de recomendar nuevos intereses a un usuario en función de sus intereses actualmente especificados.

## Curación manual

Antes de Internet, cuando necesitaba recomendaciones de libros, acudía a la biblioteca, donde había un bibliotecario disponible para sugerir libros que fueran relevantes para sus intereses o similares a los libros que le gustaron. 
Dado el número limitado de usuarios e intereses de DataSciencester, sería fácil para usted pasar una tarde recomendando manualmente los intereses de cada usuario. Pero este método no se escala particularmente bien, y está limitado por su conocimiento e imaginación personales. (No es que sugiera que su conocimiento personal e imaginación son limitados). _Así que pensemos en lo que podemos hacer con los datos._

## Recomendar lo que es popular

Un enfoque sencillo es simplemente recomendar lo que es popular:

 popular_interests = Contador (interés por interés_usuario en interés_usuario) .most_common () que se parece a:

```
[('Python', 4),
('R', 4),
('Java', 3),
('regression', 3),
('statistics', 3),
('probability', 3),
# ...
]
```
Habiendo calculado esto, solo podemos sugerirle a un usuario los intereses más populares en los que aún no está interesado:

```python
def most_popular_new_interests(user_interests, max_results=5):
    suggestions = [(interest, frequency)
                   for interest, frequency in popular_interests
                   if interest not in user_interests]
    return suggestions[:max_results]
```

Entonces, si eres usuario 1, con intereses:

```["NoSQL", "MongoDB", "Cassandra", "HBase", "Postgres"]```

Entonces te recomendaríamos:

```python
most_popular_new_interests(users_interests[1], 5)
```

Si eres usuario 3, quien ya está interesado en muchas de esas cosas, en vez de eso, obtendrás:

```
[('Java', 3),
('HBase', 3),
('Big Data', 3),
('neural networks', 2),
('Hadoop', 2)]
```

## Filtrado colaborativo basado en el usuario

Una forma de tomar en cuenta los intereses de un usuario es buscar usuarios que de alguna manera sean similares a él, y luego sugerir las cosas en las que están interesados esos usuarios. Para hacer eso, necesitaremos una manera para medir cuán similares son dos usuarios. 
Aquí usaremos una métrica llamada <code>coseno similar</code>. 

Dados dos vectores, v y w, se define como:

```python
def cosine_similarity(v, w):
    return dot(v, w) / math.sqrt(dot(v, v) * dot(w, w))
```

Mide el “ángulo” entre v y w. Si v y w apuntan en la misma dirección, entonces el numerador y el denominador son iguales, y su similitud de coseno es igual a 1. Si v y w apuntan en direcciones opuestas, entonces su similitud de coseno es igual a -1. Y si v es 0 cuando w no lo es (y viceversa), entonces el punto (v, w) es 0 y, por lo tanto, la similitud del coseno será 0.

A continuación, queremos producir un vector de "interés" de 0 y 1 para cada usuario. Solo necesitamos iterar sobre la lista unique_interests, sustituyendo un 1 si el usuario tiene cada interés, un 0 si no.

```python
def make_user_interest_vector(user_interests):
    """"dada una lista de intereses, produce un vector cuyo elemento i-th es 1
     si unique_interests [i] está en la lista, 0 de lo contrario"""
    return [1 if interest in user_interests else 0
            for interest in unique_interests]
```

Podemos crear una matriz de intereses del usuario simplemente mapeando esta función contra la lista de listas de intereses:

```python
interest_user_matrix = [[user_interest_vector[j]
                         for user_interest_vector in user_interest_matrix]
                        for j, _ in enumerate(unique_interests)]
```

Ahora user_interest_matrix \[i\] \[j\] es igual a 1 si el usuario i especificó interés j, 0 de lo contrario. Debido a que tenemos un pequeño conjunto de datos, no es un problema calcular las similitudes de pares entre todos nuestros usuarios:

```python
interest_similarities = [[cosine_similarity(user_vector_i, user_vector_j)
                          for user_vector_j in interest_user_matrix]
                         for user_vector_i in interest_user_matrix]
```                         

En particular, las similitudes de usuario \[i\] son el vector de las similitudes del usuario i con todos los demás usuarios. 
Podemos usar esto para escribir una función que encuentre los usuarios más similares a un usuario dado. Nos aseguraremos de no incluir al usuario, ni a ningún usuario con ninguna similitud. Y ordenaremos los resultados de más similares a menos similares:

```python
def most_similar_users_to(user_id):
    pairs = [(other_user_id, similarity)                      # encontrar otro
             for other_user_id, similarity in                 # usuarios con
                enumerate(user_similarities[user_id])         # no cero
             if user_id != other_user_id and similarity > 0]  # similaridad

```  

Por ejemplo, si llamamos most_similar_users_to (0) obtenemos:

``` 
[(9, 0.5669467095138409),
(1, 0.3380617018914066),
(8, 0.1889822365046136),
(13, 0.1690308509457033),
(5, 0.1543033499620919)]
``` 

**¿Cómo utilizamos esto para sugerir nuevos intereses a un usuario?** Para cada interés, podemos agregar las similitudes de los usuarios interesados en él:

```python
def user_based_suggestions(user_id, include_current_interests=False):
    # sum up the similarities
    suggestions = defaultdict(float)
    for other_user_id, similarity in most_similar_users_to(user_id):
        for interest in users_interests[other_user_id]:
            suggestions[interest] += similarity

    # convertirlos en una lista ordenada
    suggestions = sorted(suggestions.items(),
                         key=lambda pair: pair[1],
                         reverse=True)

    # y (tal vez) excluir ya los intereses
    if include_current_interests:
        return suggestions
    else:
        return [(suggestion, weight)
                for suggestion, weight in suggestions
                if suggestion not in users_interests[user_id]]
``` 

Si llamamos a user_based_suggestions (0), los primeros intereses sugeridos son:

```
[('MapReduce', 0.5669467095138409),
('MongoDB', 0.50709255283711),
('Postgres', 0.50709255283711),
('NoSQL', 0.3380617018914066),
('neural networks', 0.1889822365046136),
('deep learning', 0.1889822365046136),
('artificial intelligence', 0.1889822365046136),
#...
]
```

Estas parecen sugerencias bastante decentes para alguien cuyos intereses declarados son “Big Data” y están relacionados con la base de datos.

## Filtrado colaborativo basado en items 

Un enfoque alternativo es calcular las similitudes entre los intereses directamente. 
Luego podemos generar sugerencias para cada usuario agregando intereses que sean similares a sus intereses actuales. 
Para empezar, queremos transponer nuestra matriz de intereses de usuario para que las filas correspondan a intereses y las columnas correspondan a los usuarios:

```python
interest_user_matrix = [[user_interest_vector[j]
                         for user_interest_vector in user_interest_matrix]
                        for j, _ in enumerate(unique_interests)]
```

Por ejemplo, unique_interests [0] es Big Data, por lo que interest_user_matrix [0] es:

```[1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0]```

Ahora podemos usar la similitud de coseno de nuevo. Si precisamente los mismos usuarios están interesados en dos temas, su similitud será 1. Si no hay dos usuarios interesados en ambos temas, su similitud será 0.

```python
interest_similarities = [[cosine_similarity(user_vector_i, user_vector_j)
                          for user_vector_j in interest_user_matrix]
                         for user_vector_i in interest_user_matrix]
```

Por ejemplo, podemos encontrar los intereses más similares a Big Data (interés 0) usando:

```python
def most_similar_interests_to(interest_id):
    similarities = interest_similarities[interest_id]
    pairs = [(unique_interests[other_interest_id], similarity)
             for other_interest_id, similarity in enumerate(similarities)
             if interest_id != other_interest_id and similarity > 0]
    return sorted(pairs,
                  key=lambda pair: pair[1],
                  reverse=True)
```

Ahora podemos crear recomendaciones para un usuario al resumir las similitudes del
intereses similares a los suyos:

```python
def item_based_suggestions(user_id, include_current_interests=False):
    suggestions = defaultdict(float)
    user_interest_vector = user_interest_matrix[user_id]
    for interest_id, is_interested in enumerate(user_interest_vector):
        if is_interested == 1:
            similar_interests = most_similar_interests_to(interest_id)
            for interest, similarity in similar_interests:
                suggestions[interest] += similarity

    suggestions = sorted(suggestions.items(),
                         key=lambda pair: pair[1],
                         reverse=True)

    if include_current_interests:
        return suggestions
    else:
        return [(suggestion, weight)
                for suggestion, weight in suggestions
                if suggestion not in users_interests[user_id]]
```

Al ejecutarlo:

```python
if __name__ == "__main__":

    print("Popular Interests")
    print(popular_interests)
    print()

    print("Most Popular New Interests")
    print("already like:", ["NoSQL", "MongoDB", "Cassandra", "HBase", "Postgres"])
    print(most_popular_new_interests(["NoSQL", "MongoDB", "Cassandra", "HBase", "Postgres"]))
    print()
    print("already like:", ["R", "Python", "statistics", "regression", "probability"])
    print(most_popular_new_interests(["R", "Python", "statistics", "regression", "probability"]))
    print()

    print("User based similarity")
    print("most similar to 0")
    print(most_similar_users_to(0))

    print("Suggestions for 0")
    print(user_based_suggestions(0))
    print()

    print("Item based similarity")
    print("most similar to 'Big Data'")
    print(most_similar_interests_to(0))
    print()

    print("suggestions for user 0")
    print(item_based_suggestions(0))

```

**Resultado**

```
Popular Interests
[('Python', 4), ('R', 4), ('Big Data', 3), ('HBase', 3), ('Java', 3), ('statistics', 3), ('regression', 3), ('probability', 3), ('Hadoop', 2), ('Cassandra', 2), ('MongoDB', 2), ('Postgres', 2), ('scikit-learn', 2), ('statsmodels', 2), ('pandas', 2), ('machine learning', 2), ('libsvm', 2), ('C++', 2), ('neural networks', 2), ('deep learning', 2), ('artificial intelligence', 2), ('Spark', 1), ('Storm', 1), ('NoSQL', 1), ('scipy', 1), ('numpy', 1), ('decision trees', 1), ('Haskell', 1), ('programming languages', 1), ('mathematics', 1), ('theory', 1), ('Mahout', 1), ('MapReduce', 1), ('databases', 1), ('MySQL', 1), ('support vector machines', 1)]

Most Popular New Interests
already like: ['NoSQL', 'MongoDB', 'Cassandra', 'HBase', 'Postgres']
[('Python', 4), ('R', 4), ('Big Data', 3), ('Java', 3), ('statistics', 3)]

already like: ['R', 'Python', 'statistics', 'regression', 'probability']
[('Big Data', 3), ('HBase', 3), ('Java', 3), ('Hadoop', 2), ('Cassandra', 2)]

User based similarity
most similar to 0
[(9, 0.5669467095138409), (1, 0.3380617018914066), (8, 0.1889822365046136), (13, 0.1690308509457033), (5, 0.1543033499620919)]
Suggestions for 0
[('MapReduce', 0.5669467095138409), ('MongoDB', 0.50709255283711), ('Postgres', 0.50709255283711), ('NoSQL', 0.3380617018914066), ('neural networks', 0.1889822365046136), ('deep learning', 0.1889822365046136), ('artificial intelligence', 0.1889822365046136), ('databases', 0.1690308509457033), ('MySQL', 0.1690308509457033), ('Python', 0.1543033499620919), ('R', 0.1543033499620919), ('C++', 0.1543033499620919), ('Haskell', 0.1543033499620919), ('programming languages', 0.1543033499620919)]

Item based similarity
most similar to 'Big Data'
[('Hadoop', 0.8164965809277261), ('Java', 0.6666666666666666), ('MapReduce', 0.5773502691896258), ('Spark', 0.5773502691896258), ('Storm', 0.5773502691896258), ('Cassandra', 0.4082482904638631), ('artificial intelligence', 0.4082482904638631), ('deep learning', 0.4082482904638631), ('neural networks', 0.4082482904638631), ('HBase', 0.3333333333333333)]

suggestions for user 0
[('MapReduce', 1.861807319565799), ('MongoDB', 1.3164965809277263), ('Postgres', 1.3164965809277263), ('NoSQL', 1.2844570503761732), ('MySQL', 0.5773502691896258), ('databases', 0.5773502691896258), ('Haskell', 0.5773502691896258), ('programming languages', 0.5773502691896258), ('artificial intelligence', 0.4082482904638631), ('deep learning', 0.4082482904638631), ('neural networks', 0.4082482904638631), ('C++', 0.4082482904638631), ('Python', 0.2886751345948129), ('R', 0.2886751345948129)]
```