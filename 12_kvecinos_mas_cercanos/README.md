# k-Vecinos más cercanos

Imagina que estás tratando de predecir cómo votaré en las próximas elecciones presidenciales. Si no sabe nada más sobre mí (y si tiene los datos), un enfoque sensato es observar cómo planean votar mis vecinos. Al vivir en el centro de Seattle, como yo, mis vecinos invariablemente planean votar por el candidato demócrata, lo que sugiere que "candidato demócrata" también es una buena suposición para mí. Ahora imagina que sabes más acerca de mí que solo de la geografía; quizás conozcas mi edad, mis ingresos, cuántos hijos tengo, etc. En la medida en que mi comportamiento esté influenciado (o caracterizado) por esas cosas, mirar solo a mis vecinos que están cerca de mí entre todas esas dimensiones parece ser un predictor aún mejor que mirar a todos mis vecinos. 

##  Modelo

El modelo Vecinos más cercanos es uno de los modelos predictivos más simples que existen. 

> No hace suposiciones matemáticas, y no requiere ningún tipo de maquinaria pesada. 

Las únicas cosas que requiere son: 
* Alguna noción de distancia Una suposición de que los puntos que están cerca entre sí son similares. 

Digamos que hemos elegido un número k como 3 o 5. Luego, cuando queremos clasificar un nuevo punto de datos, encontramos los k puntos etiquetados más cercanos y les permitimos votar sobre la nueva salida. Para hacer esto, necesitaremos una función que cuente los votos. Una posibilidad es:

```python
def majority_vote(labels):
    """asume que las etiquetas se ordenan de la más cercana a la más lejana"""
    vote_counts = Counter(labels)
    winner, winner_count = vote_counts.most_common(1)[0]
    num_winners = len([count
                       for count in vote_counts.values()
                       if count == winner_count])

    if num_winners == 1:
        return winner                     # ganador único, así que devuélvelo
    else:
        return majority_vote(labels[:-1]) # intente de nuevo sin el más lejano
```

Este enfoque seguramente funcionará con el tiempo, ya que en el peor de los casos nos dirigimos a una sola etiqueta, momento en el que esa etiqueta gana.

**Clasificador**

Con esta función es fácil crear un clasificador

```python
def knn_classify(k, labeled_points, new_point):
    """cada punto etiquetado debe ser un par (punto, etiqueta)"""

    # ordenar los puntos etiquetados de más cercano a más lejano
    by_distance = sorted(labeled_points,
                         key=lambda point_label: distance(point_label[0], new_point))

    # encuentra las etiquetas para el k mas cercano
    k_nearest_labels = [label for _, label in by_distance[:k]]

    # y déjalos votar
    return majority_vote(k_nearest_labels)
```

## Ejemplo Favorite Languages

**Funciones**

```python
def mean(x):
    return sum(x) / len(x)

def squared_distance(v, w):
    return sum_of_squares(vector_subtract(v, w))

def dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)

def vector_subtract(v, w):
    """subtracts two vectors componentwise"""
    return [v_i - w_i for v_i, w_i in zip(v,w)]
```

**Contar votos**

```python
def raw_majority_vote(labels):
    votes = Counter(labels)
    winner, _ = votes.most_common(1)[0]
    return winner

def majority_vote(labels):
    """asume que las etiquetas se ordenan de la más cercana a la más lejana"""
    vote_counts = Counter(labels)
    winner, winner_count = vote_counts.most_common(1)[0]
    num_winners = len([count
                       for count in vote_counts.values()
                       if count == winner_count])

    if num_winners == 1:
        return winner                     # ganador único, así que devuélvelo
    else:
        return majority_vote(labels[:-1]) # intente de nuevo sin el más lejano
```

**Clasificador**

```python
def knn_classify(k, labeled_points, new_point):
    """cada punto etiquetado debe ser un par (punto, etiqueta)"""

    # ordenar los puntos etiquetados de más cercano a más lejano
    by_distance = sorted(labeled_points,
                         key=lambda point_label: distance(point_label[0], new_point))

    # encuentra las etiquetas para el k mas cercano
    k_nearest_labels = [label for _, label in by_distance[:k]]

    # y déjalos votar
    return majority_vote(k_nearest_labels)
```

**Dimensionalidad**

```python
def random_point(dim):
    return [random.random() for _ in range(dim)]

def random_distances(dim, num_pairs):
    return [distance(random_point(dim), random_point(dim))
            for _ in range(num_pairs)]
```

**Datos**

```bash
cities = [(-86.75,33.5666666666667,'Python'),(-88.25,30.6833333333333,'Python'),(-112.016666666667,33.4333333333333,'Java'),(-110.933333333333,32.1166666666667,'Java'),(-92.2333333333333,34.7333333333333,'R'),(-121.95,37.7,'R'),(-118.15,33.8166666666667,'Python'),(-118.233333333333,34.05,'Java'),(-122.316666666667,37.8166666666667,'R'),(-117.6,34.05,'Python'),(-116.533333333333,33.8166666666667,'Python'),(-121.5,38.5166666666667,'R'),(-117.166666666667,32.7333333333333,'R'),(-122.383333333333,37.6166666666667,'R'),(-121.933333333333,37.3666666666667,'R'),(-122.016666666667,36.9833333333333,'Python'),(-104.716666666667,38.8166666666667,'Python'),(-104.866666666667,39.75,'Python'),(-72.65,41.7333333333333,'R'),(-75.6,39.6666666666667,'Python'),(-77.0333333333333,38.85,'Python'),(-80.2666666666667,25.8,'Java'),(-81.3833333333333,28.55,'Java'),(-82.5333333333333,27.9666666666667,'Java'),(-84.4333333333333,33.65,'Python'),(-116.216666666667,43.5666666666667,'Python'),(-87.75,41.7833333333333,'Java'),(-86.2833333333333,39.7333333333333,'Java'),(-93.65,41.5333333333333,'Java'),(-97.4166666666667,37.65,'Java'),(-85.7333333333333,38.1833333333333,'Python'),(-90.25,29.9833333333333,'Java'),(-70.3166666666667,43.65,'R'),(-76.6666666666667,39.1833333333333,'R'),(-71.0333333333333,42.3666666666667,'R'),(-72.5333333333333,42.2,'R'),(-83.0166666666667,42.4166666666667,'Python'),(-84.6,42.7833333333333,'Python'),(-93.2166666666667,44.8833333333333,'Python'),(-90.0833333333333,32.3166666666667,'Java'),(-94.5833333333333,39.1166666666667,'Java'),(-90.3833333333333,38.75,'Python'),(-108.533333333333,45.8,'Python'),(-95.9,41.3,'Python'),(-115.166666666667,36.0833333333333,'Java'),(-71.4333333333333,42.9333333333333,'R'),(-74.1666666666667,40.7,'R'),(-106.616666666667,35.05,'Python'),(-78.7333333333333,42.9333333333333,'R'),(-73.9666666666667,40.7833333333333,'R'),(-80.9333333333333,35.2166666666667,'Python'),(-78.7833333333333,35.8666666666667,'Python'),(-100.75,46.7666666666667,'Java'),(-84.5166666666667,39.15,'Java'),(-81.85,41.4,'Java'),(-82.8833333333333,40,'Java'),(-97.6,35.4,'Python'),(-122.666666666667,45.5333333333333,'Python'),(-75.25,39.8833333333333,'Python'),(-80.2166666666667,40.5,'Python'),(-71.4333333333333,41.7333333333333,'R'),(-81.1166666666667,33.95,'R'),(-96.7333333333333,43.5666666666667,'Python'),(-90,35.05,'R'),(-86.6833333333333,36.1166666666667,'R'),(-97.7,30.3,'Python'),(-96.85,32.85,'Java'),(-95.35,29.9666666666667,'Java'),(-98.4666666666667,29.5333333333333,'Java'),(-111.966666666667,40.7666666666667,'Python'),(-73.15,44.4666666666667,'R'),(-77.3333333333333,37.5,'Python'),(-122.3,47.5333333333333,'Python'),(-89.3333333333333,43.1333333333333,'R'),(-104.816666666667,41.15,'Java')]
cities = [([longitude, latitude], language) for longitude, latitude, language in cities]
```

**Graficando**

```python
def plot_state_borders(plt, color='0.8'):
    pass

def plot_cities():

    # clave es el idioma, el valor es par (longitudes, latitudes)
    plots = { "Java" : ([], []), "Python" : ([], []), "R" : ([], []) }

    # queremos que cada idioma tenga un marcador y un color diferente
    markers = { "Java" : "o", "Python" : "s", "R" : "^" }
    colors  = { "Java" : "r", "Python" : "b", "R" : "g" }

    for (longitude, latitude), language in cities:
        plots[language][0].append(longitude)
        plots[language][1].append(latitude)

    # crear una serie de dispersión para cada idioma
    for language, (x, y) in plots.items():
        plt.scatter(x, y, color=colors[language], marker=markers[language],
                          label=language, zorder=10)

    plot_state_borders(plt)    # asume que tenemos una función que hace esto

    plt.legend(loc=0)          # deja matplotlib elegir la ubicación
    plt.axis([-130,-60,20,55]) # establecer los ejes
    plt.title("Favorite Programming Languages")
    plt.show()
```

![](imagenes/1_KvecinosEjemplo.png)

```python
def classify_and_plot_grid(k=1):
    plots = { "Java" : ([], []), "Python" : ([], []), "R" : ([], []) }
    markers = { "Java" : "o", "Python" : "s", "R" : "^" }
    colors  = { "Java" : "r", "Python" : "b", "R" : "g" }

    for longitude in range(-130, -60):
        for latitude in range(20, 55):
            predicted_language = knn_classify(k, cities, [longitude, latitude])
            plots[predicted_language][0].append(longitude)
            plots[predicted_language][1].append(latitude)

    # crear una serie de dispersión para cada idioma
    for language, (x, y) in plots.items():
        plt.scatter(x, y, color=colors[language], marker=markers[language],
                          label=language, zorder=0)

    plot_state_borders(plt, color='black')    # Supongamos que tenemos una función que hace esto

    plt.legend(loc=0)          # deja matplotlib elegir la ubicación
    plt.axis([-130,-60,20,55]) # establecer los ejes
    plt.title(str(k) + "-Nearest Neighbor Programming Languages")
    plt.show()
```

![](imagenes/2_KvecinosEjemplo.png)

**Ejecutándolo**

```python
if __name__ == "__main__":

    # prueba varios valores diferentes para k
    for k in [1, 3, 5, 7]:
        num_correct = 0

        for location, actual_language in cities:

            other_cities = [other_city
                            for other_city in cities
                            if other_city != (location, actual_language)]

            predicted_language = knn_classify(k, other_cities, location)

            if predicted_language == actual_language:
                num_correct += 1

        print(k, "neighbor[s]:", num_correct, "correct out of", len(cities))

    dimensions = range(1, 101, 5)

    avg_distances = []
    min_distances = []

    random.seed(0)
    for dim in dimensions:
        distances = random_distances(dim, 10000)  # 10,000 pares aleatorios
        avg_distances.append(mean(distances))     # rastrea el promedio
        min_distances.append(min(distances))      # rastrea el mínimo
        print(dim, min(distances), mean(distances), min(distances) / mean(distances))
```

**Resultado**

```
1 neighbor[s]: 40 correct out of 75
3 neighbor[s]: 44 correct out of 75
5 neighbor[s]: 41 correct out of 75
7 neighbor[s]: 35 correct out of 75
1 7.947421226228712e-06 0.3310009902894413 2.4010264196729895e-05
6 0.18647467260473205 0.9677679968196268 0.19268530600055306
11 0.315888574043911 1.3334395796543002 0.23689755341281116
16 0.7209190490469604 1.6154152410436047 0.4462747600308797
21 0.9694045860570238 1.8574960773724116 0.5218878240800003
26 1.1698067560262715 2.0632214700056446 0.5669807013122402
31 1.2930748713962408 2.257299829279505 0.5728414340991512
36 1.5123637311959328 2.437670913316559 0.620413413038717
41 1.5514668006745476 2.6039686964057926 0.5958085451703037
46 1.6688006850159558 2.756796053135482 0.6053406392242623
51 2.0135369208019926 2.902997336534375 0.6936061895274667
56 2.1422705294432887 3.0461953095695335 0.7032610557548324
61 2.2891825062886793 3.1783717877656223 0.720237486092828
66 2.3805561409678484 3.305579571524835 0.7201630121006946
71 2.428355816745725 3.4329484139337785 0.7073674066552892
76 2.5356413086431617 3.558475062222762 0.7125640237195596
81 2.682272988673655 3.669873368578009 0.7308897935388364
86 2.8348947533212074 3.779672772114365 0.7500370863415659
91 3.015796748953059 3.888554628876585 0.7755572537306314
96 2.976216447967502 3.9912782735625743 0.7456800162698157
```