def knn_classify(k, labeled_points, new_point):
    """cada punto etiquetado debe ser un par (punto, etiqueta)"""

    # ordenar los puntos etiquetados de más cercano a más lejano
    by_distance = sorted(labeled_points,
                         key=lambda point_label: distance(point_label[0], new_point))

    # encuentra las etiquetas para el k mas cercano
    k_nearest_labels = [label for _, label in by_distance[:k]]

    # y déjalos votar
    return majority_vote(k_nearest_labels)