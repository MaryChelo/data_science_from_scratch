#--Contar los votos--

def majority_vote(labels):
    """asume que las etiquetas se ordenan de la más cercana a la más lejana"""
    vote_counts = Counter(labels)
    winner, winner_count = vote_counts.most_common(1)[0]
    num_winners = len([count
                       for count in vote_counts.values()
                       if count == winner_count])

    if num_winners == 1:
        return winner                     # ganador único, así que devuélvelo
    else:
        return majority_vote(labels[:-1]) # intente de nuevo sin el más lejano