class Set:
    # estas son las funciones miembro
    # cada uno toma un primer parámetro "self" (otra convención)
    # que se refiere al objeto Set particular en uso
    def __init__(self, values=None):
        self.dict = {} # cada instancia de Set tiene su propia propiedad dict
        # que es lo que usaremos para rastrear membresías
        if values is not None:
            for value in values:
                self.add(value)
    def __repr__(self):
        """esta es la representación de cadena de un objeto Set si lo escribe en el indicador de Python o lo pasa a str ()"""
        return "Set: " + str(self.dict.keys())
    # representaremos la membresía siendo una clave en self.dict con valor True
    def add(self, value):
        self.dict[value] = True
        # valor está en el conjunto si es una clave en el diccionario
    def contains(self, value):
        return value in self.dict
    def remove(self, value):
        del self.dict[value]

s = Set([1,2,3])
s.add(4)
print (s.contains(4)) # True
s.remove(3)
print (s.contains(3)) # False