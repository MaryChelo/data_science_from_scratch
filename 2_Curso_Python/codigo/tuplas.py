#--Tuplas--
my_list = [1, 2]
my_tuple = (1, 2)
other_tuple = 3, 4
my_list[1] = 3 # my_lista es ahora [1, 3]
try:
    my_tuple[1] = 3
except TypeError:
    print ("No puede modificar una tupla")