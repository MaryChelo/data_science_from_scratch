#--Sets--
s = set()
s.add (1) # s es ahora {1}
s.add (2) # s ahora es {1, 2}
s.add (2) # s sigue siendo {1, 2}
x = len (s) # es igual a 2
y = 2 in s # es igual a True
z = 3 in s # es falso
print(s)