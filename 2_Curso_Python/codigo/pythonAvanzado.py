# Python Avanzado

#--Sort--
x = [4,1,2,3]
y = sorted(x)# es [1,2,3,4], x no se modifica
x.sort() # ahora x es [1,2,3,4]
print(x)

#--Listas de compresión--
even_numbers = [x for x in range(5) if x % 2 == 0] # [0, 2, 4]
squares = [x * x for x in range(5)] # [0, 1, 4, 9, 16]
even_squares = [x * x for x in even_numbers] # [0, 4, 16]
print(even_numbers)
print(squares)
print(even_squares)

#--Iterador yield
def lazy_range(n):
    i = 0
    while i < n:
        yield i
        i += 1

#--Random--
import random
four_uniform_randoms = [random.random() for _ in range(4)]

print(four_uniform_randoms)

#--Expresiones regulares--
import re
p = re.compile(r'\d+')
print(p.findall('12 drummers drumming, 11 pipers piping, 10 lords a-leaping'))

#--Programación orientada a objetos--
class Set:
    # estas son las funciones miembro
    # cada uno toma un primer parámetro "self" (otra convención)
    # que se refiere al objeto Set particular en uso
    def __init__(self, values=None):
        self.dict = {} # cada instancia de Set tiene su propia propiedad dict
        # que es lo que usaremos para rastrear membresías
        if values is not None:
            for value in values:
                self.add(value)
    def __repr__(self):
        """esta es la representación de cadena de un objeto Set si lo escribe en el indicador de Python o lo pasa a str ()"""
        return "Set: " + str(self.dict.keys())
    # representaremos la membresía siendo una clave en self.dict con valor True
    def add(self, value):
        self.dict[value] = True
        # valor está en el conjunto si es una clave en el diccionario
    def contains(self, value):
        return value in self.dict
    def remove(self, value):
        del self.dict[value]

s = Set([1,2,3])
s.add(4)
print (s.contains(4)) # True
s.remove(3)
print (s.contains(3)) # False