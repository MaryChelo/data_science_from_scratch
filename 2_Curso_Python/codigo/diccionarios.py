#-- Diccionario --

tweet = {
"user" : "joelgrus",
"text" : "Data Science is Awesome",
"retweet_count" : 100,
"hashtags" : ["#data", "#science", "#datascience", "#awesome", "#yolo"]
}

tweet_keys = tweet.keys() # lista de llaves
print(tweet_keys)
tweet_values = tweet.values() # lista de valores
print(tweet_values)
tweet_items = tweet.items() # lista de (key, value)
print(tweet_items)
