# Python Básico

for i in [1, 2, 3, 4, 5]:
    print (i) # primera línea en el bloque "for i"
    for j in [1, 2, 3, 4, 5]:
        print (j)  # primera linea en bloque "for j"
        print (i + j )# última línea en el bloque "para j"
        print (i) # última línea en el bloque "for i"
        print ("done looping")

#---Funciones---
def double(x):
    """quí es donde pones una cadena de documentación opcional
     Eso explica lo que hace la función.
     por ejemplo, esta función multiplica su entrada por 2 """
    return x * 2
print(double(2))

#---Excepciones--

try:
    print (0 / 0)
except ZeroDivisionError:
    print ("No se puede dividir entre cero")

#--- Listas --
x = [1, 2, 3]
x.extend([4, 5, 6]) # x es ahora [1,2,3,4,5,6]

print(x)

#---Tuplas--
my_list = [1, 2]
my_tuple = (1, 2)
other_tuple = 3, 4
my_list[1] = 3 # my_lista es ahora [1, 3]
try:
    my_tuple[1] = 3
except TypeError:
    print ("No puede modificar una tupla")

#---Diccionarios---
tweet = {
"user" : "joelgrus",
"text" : "Data Science is Awesome",
"retweet_count" : 100,
"hashtags" : ["#data", "#science", "#datascience", "#awesome", "#yolo"]
}

tweet_keys = tweet.keys() # lista de llaves
print(tweet_keys)
tweet_values = tweet.values() # lista de valores
print(tweet_values)
tweet_items = tweet.items() # lista de (key, value)
print(tweet_items)


#--Sets--
s = set()
s.add (1) # s es ahora {1}
s.add (2) # s ahora es {1, 2}
s.add (2) # s sigue siendo {1, 2}
x = len (s) # es igual a 2
y = 2 in s # es igual a True
z = 3 in s # es falso
print(s)