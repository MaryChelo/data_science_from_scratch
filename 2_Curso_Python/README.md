# Curso intensivo de Python

## Aspectos básicos

**Identación**

Python utiliza identación

```python
for i in [1, 2, 3, 4, 5]:
    print (i) # primera línea en el bloque "for i"
    for j in [1, 2, 3, 4, 5]:
        print (j)  # primera linea en bloque "for j"
        print (i + j )# última línea en el bloque "para j"
        print (i) # última línea en el bloque "for i"
        print ("done looping")
```

**Módulos**

Ciertas características de Python no están cargadas por defecto. Estos incluyen ambas características incluidas como parte del idioma, así como las funciones de terceros que descarga usted mismo.

```python
import re
my_regex = re.compile("[0-9]+", re.I)
```

Si ya tenía una referencia diferente en su código, podría usar un alias <code>as</code>:

```python
import re as regex
my_regex = regex.compile("[0-9]+", regex.I)
```

**Funciones**

Una función es una regla para tomar cero o más entradas y devolver una salida correspondiente.
Python, normalmente definimos funciones usando <code>def</code>:

```python
def double(x):
    """quí es donde pones una cadena de documentación opcional
     Eso explica lo que hace la función.
     por ejemplo, esta función multiplica su entrada por 2 """
    return x * 2
```

**Excepciones**

Cuando algo sale mal, Python genera una excepción. Si no se maneja, esto hará que tu programa se bloquee. Puedes manejarlos usando <code>try</code> y <code>except</code>

```python
try:
    print (0 / 0)
except ZeroDivisionError:
    print ("No se puede dividir entre cero")
```

**Listas**

Probablemente la estructura de datos más fundamental en Python es la lista. Una lista es simplemente una colección ordenada.

```python
integer_list = [1, 2, 3]
heterogeneous_list = ["string", 0.1, True]
list_of_lists = [ integer_list, heterogeneous_list, [] ]
list_length = len(integer_list)  # es igual a 3
list_sum = sum(integer_list) # es igual a 6
```

**Tuplas**

Las tuplas son listas de primos inmutables. Casi cualquier cosa que puedas hacer con una lista que no implique modificarla, puedes hacerlo con una tupla. Usted especifica una tupla utilizando paréntesis (o nada) en lugar de corchetes:

```python
my_list = [1, 2]
my_tuple = (1, 2)
other_tuple = 3, 4
my_list[1] = 3 # my_lista es ahora [1, 3]
try:
    my_tuple[1] = 3
except TypeError:
    print ("No puede modificar una tupla")
```

**Diccionarios**

Otra estructura de datos fundamental es un diccionario, que asocia valores con claves y le permite recuperar rápidamente el valor correspondiente a una clave dada:

```python
tweet = {
"user" : "joelgrus",
"text" : "Data Science is Awesome",
"retweet_count" : 100,
"hashtags" : ["#data", "#science", "#datascience", "#awesome", "#yolo"]
}
```

**Sets**

Conjuntos se establece otra estructura de datos, que representa una colección de elementos distintos:

```python
s = set()
s.add (1) # s es ahora {1}
s.add (2) # s ahora es {1, 2}
s.add (2) # s sigue siendo {1, 2}
x = len (s) # es igual a 2
y = 2 in s # es igual a True
z = 3 in s # es falso
```

**Sorting**

Cada lista de Python tiene un método de clasificación que lo ordena en su lugar. Si no desea desordenar su lista, puede usar la función ordenada, que devuelve una nueva lista:

```python
x = [4,1,2,3]
y = sorted(x)# es [1,2,3,4], x no se modifica
x.sort() # ahora x es [1,2,3,4]
```


**Lista de Comprensiones**

Con frecuencia, querrá transformar una lista en otra lista, seleccionando solo ciertos
Elementos, o mediante la transformación de elementos, o ambos. La forma pitónica de hacer esto es la lista.
comprensiones

```python
even_numbers = [x for x in range(5) if x % 2 == 0] # [0, 2, 4]
squares = [x * x for x in range(5)] # [0, 1, 4, 9, 16]
even_squares = [x * x for x in even_numbers] # [0, 4, 16]
```

**Generadores e iteradores**

Un generador es algo que puede repetir. Una forma de crear generadores es con las funciones y el operador <code>yield</code>

```python
def lazy_range(n):
    i = 0
    while i < n:
        yield i
        i += 1
```

**Random**

A medida que aprendemos ciencia de datos, con frecuencia necesitaremos generar números aleatorios, lo que podemos hacer con el módulo aleatorio.

```python
import random
four_uniform_randoms = [random.random() for _ in range(4)]

```

**Expresiones regulares**

Las expresiones regulares proporcionan una forma de buscar texto. Son increíblemente útiles pero también bastante complicados, tanto que hay libros enteros escritos sobre ellos. 

```python
import re
p = re.compile(r'\d+')
p.findall('12 drummers drumming, 11 pipers piping, 10 lords a-leaping')
```

**Programación orientada a objetos**

Python le permite definir clases que encapsulan datos y las funciones que operan en ellos. Los usaremos a veces para hacer nuestro código más limpio y simple. Mediante <code>def __init__(self, values=None)</code>

```python
class Set:
    # estas son las funciones miembro
    # cada uno toma un primer parámetro "self" (otra convención)
    # que se refiere al objeto Set particular en uso
    def __init__(self, values=None):
        self.dict = {} # cada instancia de Set tiene su propia propiedad dict
        # que es lo que usaremos para rastrear membresías
        if values is not None:
            for value in values:
                self.add(value)
    def __repr__(self):
        """esta es la representación de cadena de un objeto Set si lo escribe en el indicador de Python o lo pasa a str ()"""
        return "Set: " + str(self.dict.keys())
    # representaremos la membresía siendo una clave en self.dict con valor True
    def add(self, value):
        self.dict[value] = True
        # valor está en el conjunto si es una clave en el diccionario
    def contains(self, value):
        return value in self.dict
    def remove(self, value):
        del self.dict[value]
```