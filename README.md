# [Principios básicos de Ciencia de Datos con Python](https://marychelo.gitlab.io/data_science_from_scratch/)

## Índice

* [Introducción](/1_Introduccion/README.md)
* [Curso de Python](/2_Curso_Python/README.md)
* [Visualización de datos](/3_Visualizando_datos/README.md)
* [Algebra líneal](/4_Algebra_lineal/README.md)
* [Estadística](/5_Estadistica/README.md)
* [Hipótesis e inferencia](/7_Hipotesis_Inferencia/README.md)
* [Descenso de Gradiente](/8_Descenso_de_Gradiente/README.md)
* [Obteniendo datos](/9_Obteniendo_datos/README.md)
* [Trabajando con datos](/10_Trabajando_con_datos/README.md)
* [Machine Learning](/11_Machine_Learning/README.md)
* [k-Vecinos más cercanos](/12_kvecinos_mas_cercanos/README.md)
* [Naive Bayes](/13_Naive_Bayes/README.md)
* [Sistemas de recomendación](/14_Sistemas_recomendacion/README.md)

---

## Acerca del libro 🙊

**Data Science from Scratch**: Primeros principios con Python 🐍

Este libro está dirigido principalmente a programadores intermedios que están interesados ​​en iniciarse en el aprendizaje automático y la ciencia de datos. Proporciona una breve introducción a Python y cubre temas básicos de matemática, recopilación de datos y visualización de datos.

_El objetivo del autor es ayudarte a desarrollar las habilidades para hacer ciencia de datos mientras te sientes cómodo con las matemáticas y estadística involucrados en el núcleo de la ciencia de datos._


## Ciencia de datos 😍

> El científico de datos ha sido llamado "el trabajo más sexy del siglo XXI".

La ciencia de datos se encuentra en la intersección de:
* Habilidades de hacking.
* Conocimientos de matemáticas.
* Conocimientos de estadística.
* Experiencia sustantiva.

## Comenzando desde <code>cero</code> 🚀

Se utilizan marcos, módulos, herramientas y técnicas algorítmicas de ciencia de datos como: NumPy, scikit-learn y pandas, siendo éstas una buena manera de comenzar.

## ¿Por qué Python? 💪

* Sencillo de codificar y de comprender.
* Contiene una gran variedad de librerías útiles relacionadas con la ciencia de datos.
* Es compatible con la programación orientada a objetos, la programación estructurada y los patrones de programación funcional.

## Prácticas del libro 💡

En este repositorio se encuentran algunos ejemplos y códigos del libro **Data Science from Scratch**  implementados para la versión: `python 3.6.4`. 

* Cada uno puede ser importado como módulo:

```python
v=[1,2,3]
w=[4,5,6]

def vector_add(v, w):
    return [v_i + w_i for v_i, w_i in zip(v, w)
vector_add(v,w)
```

* Cada uno puede ejecutarse desde la línea de comandos:

```bash
python ejemplo.py
```

* Se han agregado los ejemplos para ejecutarlos en <code>Jupyter Notebook</code>

Solo requeres bajar los Notebooks y correrlos.
```bash
jupyter notebook
```

> Para descargar el libro [Data Science from Scratch First Principles with Python](http://file.allitebooks.com/20150707/Data%20Science%20from%20Scratch-%20First%20Principles%20with%20Python.pdf)

> Si quieres encontrar todos los códigos puedes ingresar al [github](https://github.com/joelgrus/data-science-from-scratch) del autor del libro.

--- 

## Instalación de Python 🔧

`Windows` <br/>

Mediante el instalador ejecutable python-3.6.4.exe se puede realizar la instalación. <br/>
Se debe configurar la **variable path**.

```
C:\Users\nameUser\AppData\Local\Programs\Python\Python36-32;
C:\Users\nameUser\AppData\Local\Programs\Python\Python36-32\Scripts;
```

`Linux` <br/>

Si está utilizando Ubuntu 16.10 o más reciente, puede instalar fácilmente Python 3.6 con los siguientes comandos:

```shell
$ sudo apt-get update
$ sudo apt-get install python3.6
```

`Mac OS X` <br/>

Se puede instalar Python 3 en macOS mediante un gestor de paquetes Homebrew.

```shell
brew install python3
```

**Instalación de Anaconda**

Si realizas la instalación de Anaconda por defecto trae la herramienta de JupyterNotebook.

`Windows` <br/>


1. Descargar instalador de Anaconda para [Windows](https://www.anaconda.com/download/#windows).

2. Haga doble clic en el instalador para iniciar, clic y siguiente.
3. Lea los términos de la licencia y haga clic en "Acepto".

`Linux` <br/>

1. Descargar desde la página oficial de [Anaconda](http://docs.anaconda.com/anaconda/install/linux/)

2. Ejecuta lo siguiente:

```bash
md5sum /path/filename

```

3. Anaconda para python 3.6

```bash
bash ~/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh
export PATH=~/anaconda3/bin:$PATH
```

4. Ejecutar en la terminal

```shell
export PATH=~/anaconda3/bin:$PATH
anaconda-navigator
```

`Mac OS X` <br/>

1. Descargue el instalador gráfico de [MacOS](http://docs.anaconda.com/anaconda/install/mac-os/#macos-graphical-install) para su versión de Python.
2. Haga doble clic en el archivo descargado y haga clic en continuar para iniciar la instalación.
3. Responda las indicaciones en las pantallas Introducción, Léame y Licencia.
4. Haga clic en el botón Instalar para instalar Anaconda en su directorio de usuarios.
5. Ejecutar en la terminal para Python 3.6

```bash
bash ~/Downloads/Anaconda3-5.3.0-MacOSX-x86_64.sh
```

# Licencia 💼

[The Unlicense](LICENSE.txt)

# Referencias 👀
Grus, J. (2015). Data science from scratch: first principles with python. " O'Reilly Media, Inc.".
