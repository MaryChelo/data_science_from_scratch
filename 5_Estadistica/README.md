# Estadística y probabilidad

## Estadística

_La estadística se refiere a las matemáticas y técnicas con las que entendemos los datos._

Una descripción obvia de cualquier conjunto de datos es simplemente los datos en sí:

```python
num_friends = [100, 49, 41, 40, 25, 100...]
```

Para un conjunto de datos lo suficientemente pequeño, esta podría ser la mejor descripción. Pero para un conjunto de datos más grande, esto es difícil de manejar y probablemente opaco. (Imagínese mirando una lista de 1 millón de números). Por ese motivo, utilizamos estadísticas para extraer y comunicar las características relevantes de nuestros datos. 

```python
friend_counts = collections.Counter(num_friends)
xs = range(100) # el mayor valor es 100
ys = [friend_counts[x] for x in xs] # la altura es solo # de amigos
plt.bar(xs, ys)
plt.axis([0, 101, 0, 25])
plt.title("Histogram of Friend Counts")
plt.xlabel("# of friends")
plt.ylabel("# of people")
plt.show()
```

![](imagenes/10_histograma.png)

**Mediana**

Es el valor más intermedio (si el número de puntos de datos es impar).

```python
def median(v):
    """encuentra el valor 'medio' de v"""
    n = len(v)
    sorted_v = sorted(v)
    midpoint = n // 2
    if n % 2 == 1:
        # si es impar, devuelve el valor medio
        return sorted_v[midpoint]
    else:
        # si es que, devuelve el promedio de los valores medios
        lo = midpoint - 1
        hi = midpoint
        return (sorted_v[lo] + sorted_v[hi]) / 2
median(num_friends) 
```

**Percentil**

Una generalización de la mediana es el cuantil, que representa el valor menor al que se encuentra un determinado percentil de los datos. (La mediana representa el valor inferior al 50% de los datos).


```python
def quantile(x, p):
    """devuelve el valor pth-percentil en x"""
    p_index = int(p * len(x))
    return sorted(x)[p_index]
```

**Dispersión** 
 
Dispersión se refiere a las medidas de la distribución de nuestros datos. Por lo general, son estadísticas para las cuales los valores cercanos a cero significan que no están distribuidos en absoluto y para los cuales los valores grandes (lo que sea que signifique) significan que están muy dispersos. Por ejemplo, una medida muy simple es el rango, que es la diferencia entre los elementos más grandes y más pequeños:

```python
def data_range(x):
    return max(x) - min(x)
data_range(num_friends)
```

**Media** 

```python
import numpy
def de_mean(x):
    """raduce x al restar su media (por lo que el resultado tiene una media de 0)"""
    x_bar = numpy.mean(x)
    return [x_i - x_bar for x_i in x]
```

**Varianza** 

```python
def sum_of_squares(v):
    """calcula la suma de los elementos cuadrados en v"""
    return sum(v_i ** 2 for v_i in v)
def variance(x):
    """asume que x tiene al menos dos elementos"""
    n = len(x)
    deviations = de_mean(x)
    return sum_of_squares(deviations) / (n - 1)
variance(num_friends) 
```

## Probabilidad

_P(E) significa "la probabilidad del evento E"_

Usaremos la teoría de la probabilidad para construir modelos. Usaremos la teoría de la probabilidad para evaluar modelos. Usaremos la teoría de la probabilidad en todo el lugar.

**Dependencia e independencia**

En términos generales, decimos que dos eventos E y F son dependientes si saber algo sobre si sucede o no, nos da información sobre si ocsurre o no (y viceversa). De lo contrario son independientes. 

<br/>

Por ejemplo, si lanzamos una moneda normal dos veces, saber si el primer lanzamiento es Heads no nos da información sobre si el segundo lanzamiento es Heads. Estos eventos son independientes. 

_Matemáticamente, decimos que dos eventos E y F son independientes si la probabilidad de que ambos ocurran es el producto de las probabilidades de que ocurra cada uno_

![](imagenes/1_probabilidad.png)

**Probabilidad condicional**

* Cuando dos eventos E y F son independientes, entonces por definición tenemos:

![](imagenes/2_probabilidadCondicional.png)

* Si no son necesariamente independientes (y si la probabilidad de F no es cero), definimos la probabilidad de E "condicional a F" como:

![](imagenes/3_probabilidadCondicional2.png)

Un ejemplo complicado común involucra a una familia con dos hijos (desconocidos). Si asumimos que:
1. Cada niño es igualmente probable que sea un niño o una niña.
2. El género del segundo niño es independiente del género del primer niño, entonces el evento "no niñas" tiene una probabilidad de 1/4, el el evento "una niña, un niño" tiene una probabilidad 1/2, y el evento "dos niñas" tiene una probabilidad 1/4. 

Ahora podemos preguntar cuál es la probabilidad de que el evento “ambos niños son niñas” (B) condicional al evento “el niño mayor es una niña” (G)? Usando la definición de probabilidad condicional:

* Ya que el evento B y G (“ambos niños son niñas y el mayor es una niña”) es solo el evento B. (Una vez que sepa que ambos niños son niñas, es necesariamente cierto que el niño mayor es una niña).

![](imagenes/4_probabilidadCondicionalEjemplo.png)

* También podríamos preguntar acerca de la probabilidad de que el evento "ambos niños son niñas" condicional al evento "al menos uno de los niños es una niña" (L).

![](imagenes/5_probabilidadCondicionalEjemploNiñas.png)

**¿Cómo puede ser este el caso?** Bueno, si todo lo que sabe es que al menos uno de los niños es una niña, entonces es dos veces más probable que la familia tenga un niño y una niña que la de las dos niñas.

```python
import random
def random_kid():
    return random.choice(["boy", "girl"])

both_girls = 0
older_girl = 0
either_girl = 0
random.seed(0)
for _ in range(10000):
    younger = random_kid()
    older = random_kid()
    if older == "girl":
        older_girl += 1
    if older == "girl" and younger == "girl":
        both_girls += 1
    if older == "girl" or younger == "girl":
        either_girl += 1
print ("P(both | older):", both_girls / older_girl )# 0.514 ~ 1/2
print ("P(both | either): ", both_girls / either_girl) # 0.342 ~ 1/3
```

**Distribución continua**

Un lanzamiento de moneda corresponde a una distribución discreta, una que asocia probabilidad positiva con resultados discretos. A menudo querremos modelar distribuciones a través de una serie de resultados.

Por ejemplo, la distribución uniforme pone el mismo peso en todos los números entre 0 y 1. Porque hay infinitos números entre 0 y 1, esto significa que el peso que asigna a puntos individuales necesariamente debe ser cero.

_Por esta razón, representamos una distribución continua con una **función de densidad de probabilidad (pdf)** de tal manera que la probabilidad de ver un valor en un cierto intervalo es igual a la integral de la función de densidad sobre el intervalo._

```python
def uniform_pdf(x):
    return 1 if x >= 0 and x < 1 else 0
```

La probabilidad de que una variable aleatoria que sigue esa distribución esté entre 0.2 y 0.3 es 1/10.
El random.random () de Python es una variable [pseudo] aleatoria con una densidad uniforme. A menudo nos interesará más la función de distribución acumulativa (cdf), que da la probabilidad de que una variable aleatoria sea menor o igual que un determinado valor. 

Devuelve la probabilidad de que una variable aleatoria uniforme sea <= x

```python
def uniform_cdf(x):
    "devuelve la probabilidad de que una variable aleatoria uniforme sea <= x"
    if x < 0: return 0 # uniform random nunca es menor que 0
    elif x < 1: return x # e.g. P (X <= 0.4) = 0.4
    else: return 1 # uniform random siempre es menor que 1
```

**Distribución normal**

La distribución normal es el rey de las distribuciones. Es la distribución clásica en forma de curva de campana y está completamente determinada por dos parámetros: su media (mu) y su desviación estándar (sigma). La media indica dónde está centrada la campana y la desviación estándar de cuán "ancha" es. Tiene la función de distribución:

![](imagenes/6_distribucionNormal.png)

```python
import math
def normal_pdf(x, mu=0, sigma=1):
    sqrt_two_pi = math.sqrt(2 * math.pi)
    return (math.exp(-(x-mu) ** 2 / 2 / sigma ** 2) / (sqrt_two_pi * sigma))
```

```python
from matplotlib import pyplot as plt

xs = [x / 10.0 for x in range(-50, 50)]
plt.plot(xs,[normal_pdf(x,sigma=1) for x in xs],'-',label='mu=0,sigma=1')
plt.plot(xs,[normal_pdf(x,sigma=2) for x in xs],'--',label='mu=0,sigma=2')
plt.plot(xs,[normal_pdf(x,sigma=0.5) for x in xs],':',label='mu=0,sigma=0.5')
plt.plot(xs,[normal_pdf(x,mu=-1) for x in xs],'-.',label='mu=-1,sigma=1')
plt.legend()
plt.title("Various Normal pdfs")
plt.show()
```

![](imagenes/7_graficoDistribucionNormal.png)

**Distribución normal estándar**

Si Z es una variable aleatoria normal estándar, resulta que: también es normal pero con media y desviación estándar. Por el contrario, si X es una variable aleatoria normal con media y desviación estándar, es una variable normal estándar. La función de distribución acumulativa para la distribución normal no se puede escribir de manera "elemental", pero podemos escribirla usando math.erf de Python.

![](imagenes/8_distribucionNormalEstandar.png)


```python
def normal_cdf(x, mu=0,sigma=1):
    return (1 + math.erf((x - mu) / math.sqrt(2) / sigma)) / 2
```

```python
xs = [x / 10.0 for x in range(-50, 50)]
plt.plot(xs,[normal_cdf(x,sigma=1) for x in xs],'-',label='mu=0,sigma=1')
plt.plot(xs,[normal_cdf(x,sigma=2) for x in xs],'--',label='mu=0,sigma=2')
plt.plot(xs,[normal_cdf(x,sigma=0.5) for x in xs],':',label='mu=0,sigma=0.5')
plt.plot(xs,[normal_cdf(x,mu=-1) for x in xs],'-.',label='mu=-1,sigma=1')
plt.legend(loc=4) # bottom right
plt.title("Various Normal cdfs")
plt.show()
```

![](imagenes/9_graficoDistribucionNormalEstandar.png)
