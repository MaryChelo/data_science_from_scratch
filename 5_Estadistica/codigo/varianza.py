import numpy
def sum_of_squares(v):
    """calcula la suma de los elementos cuadrados en v"""
    return sum(v_i ** 2 for v_i in v)
def de_mean(x):
    """reduce x al restar su media (por lo que el resultado tiene una media de 0)"""
    x_bar = numpy.mean(x)
    return [x_i - x_bar for x_i in x]
def variance(x):
    """asume que x tiene al menos dos elementos"""
    n = len(x)
    deviations = de_mean(x)
    return sum_of_squares(deviations) / (n - 1)
variance(num_friends) 