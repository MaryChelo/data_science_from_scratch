def uniform_cdf(x):
    "devuelve la probabilidad de que una variable aleatoria uniforme sea <= x"
    if x < 0: return 0 # uniform random nunca es menor que 0
    elif x < 1: return x # e.g. P (X <= 0.4) = 0.4
    else: return 1 # uniform random siempre es menor que 1