def median(v):
    """encuentra el valor 'medio' de v"""
    n = len(v)
    sorted_v = sorted(v)
    midpoint = n // 2
    if n % 2 == 1:
        # si es impar, devuelve el valor medio
        return sorted_v[midpoint]
    else:
        # si es que, devuelve el promedio de los valores medios
        lo = midpoint - 1
        hi = midpoint
        return (sorted_v[lo] + sorted_v[hi]) / 2
median(num_friends) 
