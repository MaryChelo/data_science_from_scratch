num_friends = [100, 49, 41, 40, 25,100, 49, 41, 40, 25, 40, 25,100, 49, 41, 40, 25, 40, 25,100, 49, 41, 40, 25, 40, 25,100, 49, 41, 40, 25, 40, 55,100, 49, 45, 40, 25, 40, 25,88, 49, 41, 40, 25, 43, 25,100, 49, 41, 40, 25, 40, 25,100, 49, 41, 40, 25]
def quantile(x, p):
    """returns the pth-percentile value in x"""
    p_index = int(p * len(x))
    return sorted(x)[p_index]
quantile(num_friends, 0.10) 
quantile(num_friends, 0.90)