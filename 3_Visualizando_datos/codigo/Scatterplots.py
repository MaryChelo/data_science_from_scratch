from matplotlib import pyplot as plt

friends = [ 70, 65, 72, 63, 71, 64, 60, 64, 67]
minutes = [175, 170, 205, 120, 220, 130, 105, 145, 190]
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
plt.scatter(friends, minutes)
# etiqueta cada punto
for label, friend_count, minute_count in zip(labels, friends, minutes):
    plt.annotate(label,
    xy=(friend_count, minute_count), # pone la etiqueta con su punto
    xytext=(5, -5), # pero ligeramente desplazado
    textcoords='offset points')
plt.title ("Minutos diarios vs. Número de amigos")
plt.xlabel ("# de amigos")
plt.ylabel ("minutos diarios dedicados al sitio")
plt.show()