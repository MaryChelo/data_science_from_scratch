from matplotlib import pyplot as plt
import collections

variance = [1, 2, 4, 8, 16, 32, 64, 128, 256]
bias_squared = [256, 128, 64, 32, 16, 8, 4, 2, 1]
total_error = [x + y for x, y in zip(variance, bias_squared)]
xs = [i for i, _ in enumerate(variance)]
# podemos hacer múltiples llamadas a plt.plot
# para mostrar múltiples series en el mismo gráfico
plt.plot(xs, variance, 'g-', label='variance') # línea sólida verde
plt.plot(xs, bias_squared, 'r-.', label='bias^2') # línea punteada roja punteada
plt.plot(xs, total_error, 'b:', label='total error') # línea punteada azul
# porque hemos asignado etiquetas a cada serie
# podemos obtener una leyenda gratis
# loc = 9 significa "centro superior"
plt.legend(loc=9)
plt.xlabel("model complexity")
plt.title("The Bias-Variance Tradeoff")
plt.show()

