from matplotlib import pyplot as plt
import collections

grades = [83,95,91,87,70,0,85,82,100,67,73,77,0]
decile = lambda grade: grade // 10 * 10
histogram = collections.Counter(decile(grade) for grade in grades)
plt.bar([x - 4 for x in histogram.keys()], # desplaza cada barra a la izquierda en 4
histogram.values(), # dan a cada barra su altura correcta
8)  # dale a cada barra un ancho de 8
plt.axis([-5, 105, 0, 5]) # eje x de -5 a 105,
# y eje de 0 a 5
plt.xticks([10 * i for i in range(11)]) # etiquetas de eje x en 0, 10, ..., 100
plt.xlabel("Decile")
plt.ylabel("# of Students")
plt.title("Distribution of Exam 1 Grades")
plt.show()