# Visualización de datos con Python

_Una parte fundamental del conjunto de herramientas del científico de datos es la visualización de datos._

Hay dos usos principales para la visualización de datos:
* Explorar datos
* Comunicar datos
## Matplotlib 

Usaremos la biblioteca matplotlib, que se usa ampliamente

```python
from matplotlib import pyplot as plt
years = [1950, 1960, 1970, 1980, 1990, 2000, 2010]
gdp = [300.2, 543.3, 1075.9, 2862.5, 5979.6, 10289.7, 14958.3]
# crear un gráfico de líneas, años en el eje x, gdp en el eje y
plt.plot(years, gdp, color='green', marker='o', linestyle='solid')
# añade un título
plt.title("Nominal GDP")
# agregar una etiqueta al eje y
plt.ylabel("Billions of $")
plt.show()
```

![](imagenes/1_Grafico.png)

**Gráfico de barras**

Un gráfico de barras es una buena opción cuando desea mostrar cómo varía una cantidad entre un conjunto discreto de elementos. 

```python
movies = ["Annie Hall", "Ben-Hur", "Casablanca", "Gandhi", "West Side Story"]
num_oscars = [5, 11, 3, 8, 10]
# las barras tienen un ancho predeterminado de 0.8, por lo que agregaremos 0.1 a las coordenadas de la izquierda
# para que cada barra quede centrada
xs = [i + 0.1 for i, _ in enumerate(movies)]
# trazar barras con coordenadas x izquierdas [xs], alturas [num_oscars]
plt.bar(xs, num_oscars)
plt.ylabel("# Premios de la academia")
plt.title("Mis peliculas favoritas")
# etiqueta eje x con nombres de películas en centros de barras
plt.xticks([i + 0.5 for i, _ in enumerate(movies)], movies)
plt.show()
```

![](imagenes/2_barras.png)

Un gráfico de barras también puede ser una buena opción para trazar histogramas de valores numéricos agrupados, a fin de explorar visualmente cómo se distribuyen los valores:

```python
from matplotlib import pyplot as plt
import collections

grades = [83,95,91,87,70,0,85,82,100,67,73,77,0]
decile = lambda grade: grade // 10 * 10
histogram = collections.Counter(decile(grade) for grade in grades)
plt.bar([x - 4 for x in histogram.keys()], # desplaza cada barra a la izquierda en 4
histogram.values(), # dan a cada barra su altura correcta
8)  # dale a cada barra un ancho de 8
plt.axis([-5, 105, 0, 5]) # eje x de -5 a 105,
# y eje de 0 a 5
plt.xticks([10 * i for i in range(11)]) # etiquetas de eje x en 0, 10, ..., 100
plt.xlabel("Decile")
plt.ylabel("# of Students")
plt.title("Distribution of Exam 1 Grades")
plt.show()
```

![](imagenes/3_histograma.png)

**Gráfico de líneas**

```python
variance = [1, 2, 4, 8, 16, 32, 64, 128, 256]
bias_squared = [256, 128, 64, 32, 16, 8, 4, 2, 1]
total_error = [x + y for x, y in zip(variance, bias_squared)]
xs = [i for i, _ in enumerate(variance)]
# podemos hacer múltiples llamadas a plt.plot
# para mostrar múltiples series en el mismo gráfico
plt.plot(xs, variance, 'g-', label='variance') # línea sólida verde
plt.plot(xs, bias_squared, 'r-.', label='bias^2') # línea punteada roja punteada
plt.plot(xs, total_error, 'b:', label='total error') # línea punteada azul
# porque hemos asignado etiquetas a cada serie
# podemos obtener una leyenda gratis
# loc = 9 significa "centro superior"
plt.legend(loc=9)
plt.xlabel("model complexity")
plt.title("The Bias-Variance Tradeoff")
plt.show()
```

![](imagenes/4_graficoLineas.png)

**Gráfico Scatterplots**

Un diagrama de dispersión es la opción correcta para visualizar la relación entre dos conjuntos de datos emparejados. 

```python
from matplotlib import pyplot as plt

friends = [ 70, 65, 72, 63, 71, 64, 60, 64, 67]
minutes = [175, 170, 205, 120, 220, 130, 105, 145, 190]
labels = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
plt.scatter(friends, minutes)
# etiqueta cada punto
for label, friend_count, minute_count in zip(labels, friends, minutes):
    plt.annotate(label,
    xy=(friend_count, minute_count), # pone la etiqueta con su punto
    xytext=(5, -5), # pero ligeramente desplazado
    textcoords='offset points')
plt.title ("Minutos diarios vs. Número de amigos")
plt.xlabel ("# de amigos")
plt.ylabel ("minutos diarios dedicados al sitio")
plt.show()
```

![](imagenes/5_Scatterplots.png)

