## Instalación 🔧

**Instalación de Python**
`Windows` <br/>

Mediante el instalador ejecutable python-3.6.4.exe se puede realizar la instalación. <br/>
Se debe configurar la **variable path**.

```
C:\Users\nameUser\AppData\Local\Programs\Python\Python36-32;
C:\Users\nameUser\AppData\Local\Programs\Python\Python36-32\Scripts;
```

`Linux` <br/>

Si está utilizando Ubuntu 16.10 o más reciente, puede instalar fácilmente Python 3.6 con los siguientes comandos:

```shell
$ sudo apt-get update
$ sudo apt-get install python3.6
```

`Mac OS X` <br/>

Se puede instalar Python 3 en macOS mediante un gestor de paquetes Homebrew.

```shell
brew install python3
```

**Instalación de Anaconda**

Si realizas la instalación de Anaconda por defecto trae la herramienta de JupyterNotebook.

`Windows` <br/>


1. Descargar instalador de Anaconda para [Windows](https://www.anaconda.com/download/#windows).

2. Haga doble clic en el instalador para iniciar.
> Si tiene problemas durante la instalación, desactive temporalmente su software antivirus durante la instalación y, a continuación, vuelva a habilitarlo una vez que finalice la instalación.
3. Haga clic en Siguiente.
4. Lea los términos de la licencia y haga clic en "Acepto".

`Linux` <br/>

1. Descargar desde la página oficial de [Anaconda](http://docs.anaconda.com/anaconda/install/linux/)

2. Ejecuta lo siguiente:

```bash
md5sum /path/filename

```
3. Anaconda para python 3.6

```bash
bash ~/Downloads/Anaconda3-5.3.0-Linux-x86_64.sh

```
4. Ejecutar en la terminal

```shell
export PATH=~/anaconda3/bin:$PATH

```

```shell
anaconda-navigator

```

`Mac OS X` <br/>

1. Descargue el instalador gráfico de [MacOS](http://docs.anaconda.com/anaconda/install/mac-os/#macos-graphical-install) para su versión de Python.
2. Haga doble clic en el archivo descargado y haga clic en continuar para iniciar la instalación.
3. Responda las indicaciones en las pantallas Introducción, Léame y Licencia.
4. Haga clic en el botón Instalar para instalar Anaconda en su directorio de usuarios domésticos (recomendado):
5. Ejecutar en la terminal para Python 3.6

```bash
bash ~/Downloads/Anaconda3-5.3.0-MacOSX-x86_64.sh
```