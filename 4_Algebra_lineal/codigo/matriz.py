A = [[1, 2, 3], # A tiene 2 filas y 3 columnas
[4, 5, 6]]
B = [[1, 2], # B tiene 3 filas y 2 columnas
[3, 4],
[5, 6]]

print(A, B)

def shape(A):
    num_rows = len(A)
    num_cols = len(A[0]) if A else 0 # número de elementos en primera fila
    return num_rows, num_cols
shape(A)