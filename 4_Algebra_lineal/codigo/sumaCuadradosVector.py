height_weight_age = [70,170, 40 ]
grades = [95, 80, 75, 62 ] 

def dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)
    
sum_of_squares(height_weight_age)