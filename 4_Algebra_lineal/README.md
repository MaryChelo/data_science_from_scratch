# Algebra Líneal

## Vectores 

_El álgebra lineal es la rama de las matemáticas que se ocupa de los espacios vectoriales_

<br/>

Los vectores son objetos que se pueden sumar (para formar nuevos vectores) y que se pueden multiplicar por escalares (es decir, números), también para formar nuevos vectores. Concretamente (para nosotros), los vectores son puntos en algún espacio finito-dimensional. <br/>

Aunque es posible que no piense en sus datos como vectores, son una buena manera de representar datos numéricos. 
Por ejemplo:
* Si tiene las alturas, los pesos y las edades de un gran número de personas, puede tratar sus datos como vectores tridimensionales (altura, peso, edad).
* Si está enseñando una clase con cuatro exámenes, puede tratar las calificaciones de los estudiantes como vectores de cuatro dimensiones (examen 1, examen 2, examen 3, examen 4).
* El enfoque más simple desde cero es representar vectores como listas de números. Una lista de tres números corresponde a un vector en el espacio tridimensional, y viceversa:

```python
height_weight_age = [70, # pulgadas,
170, # libras,
40 ] # años
grades = [95, # exam1
80, # exam2
75, # exam3
62 ] # exam4
```

Debido a que las listas de Python no son vectores (y, por lo tanto, no proporcionan facilidades para la aritmética de vectores), necesitaremos construir estas herramientas aritméticas nosotros mismos. 

<br/>

**Suma de vectores**

Así que vamos a empezar con eso. Para empezar, con frecuencia necesitaremos agregar dos vectores. Los vectores agregan por componentes. Esto significa que si dos vectores v y w tienen la misma longitud, su suma es solo el vector cuyo primer elemento es v [0] + w [0], cuyo segundo elemento es v [1] + w [1], y así en. (Si no tienen la misma longitud, entonces no podemos agregarlos). Por ejemplo, agregar los vectores [1, 2] y [2, 1] da como resultado [1 + 2, 2 + 1] o [3, 3]: <br/>

![](imagenes/1_vectores.png)

Podemos implementar esto fácilmente comprimiendo los vectores juntos y usando una lista de comprensión para agregar los elementos correspondientes: 

```python
def vector_add(v, w):
    """añade elementos correspondientes"""
    return [v_i + w_i for v_i, w_i in zip(v, w)]
```

>[165, 250, 115]

Para restar los elementos de 2 vectores:

```python
def vector_subtract(v, w):
    """resta elementos correspondientes"""
    return [v_i - w_i for v_i, w_i in zip(v, w)]
```

>[-25, 90, -35]

Para sumar una lista de vectores:

```python
def vector_sum(vectors):
    """sums all corresponding elements"""
    result = vectors[0] # comienza con el primer vector
    for vector in vectors[1:]: # luego repita sobre los demás
        result = vector_add(result, vector) # y añádalos al resultado
    return result
```

Suma de cuadrados de un vector:

```python
def dot(v, w):
    """v_1 * w_1 + ... + v_n * w_n"""
    return sum(v_i * w_i for v_i, w_i in zip(v, w))

def sum_of_squares(v):
    """v_1 * v_1 + ... + v_n * v_n"""
    return dot(v, v)
```

![](imagenes/2_cuadradoVectores.png)

**Magnitud**

Que podemos usar para calcular su magnitud (o longitud): 

```python
import math
def magnitude(v):
    return math.sqrt(sum_of_squares(v)) # math.sqrt es una función de raíz cuadrada
```

**Distancia entre dos vectores**

```python
def squared_distance(v, w):
    """(v_1 - w_1) ** 2 + ... + (v_n - w_n) ** 2"""
    return sum_of_squares(vector_subtract(v, w))
def distance(v, w):
    return math.sqrt(squared_distance(v, w))
```

![](imagenes/3_distanciaVectores.png)

## Matrices

Una matriz es una colección bidimensional de números. 

Representaremos las matrices como listas de listas, y cada lista interna tendrá el mismo tamaño y representará una fila de la matriz. 
_Si A es una matriz, entonces A [i] [j] es el elemento en la fila i y la columna jth. Por convención matemática, normalmente usaremos letras mayúsculas para representar matrices_

```python
A = [[1, 2, 3], # A tiene 2 filas y 3 columnas
[4, 5, 6]]
B = [[1, 2], # B tiene 3 filas y 2 columnas
[3, 4],
[5, 6]]
```

Dada esta representación de lista de listas, la matriz A tiene filas len (A) y columnas len (A [0]), que consideramos su forma:

```python
def shape(A):
    num_rows = len(A)
    num_cols = len(A[0]) if A else 0 # número de elementos en primera fila
    return num_rows, num_cols
```

También queremos poder crear una matriz dada su forma y una función para generar sus elementos. Podemos hacer esto usando una lista de comprensión anidada:

```python
def make_matrix(num_rows, num_cols, entry_fn):
    """devuelve una matriz num_rows x num_cols
    cuya (i, j) th entrada es entry_fn (i, j)"""
    return [[entry_fn(i, j) # dado i, crea una lista
        for j in range(num_cols)] # [entry_fn (i, 0), ...]
        for i in range(num_rows)]  # crea una lista para cada i
```

```python
def is_diagonal(i, j):
    """1 está en la 'diagonal', 0 está en todas partes"""
    return 1 if i == j else 0
```

Para ejecutar la función

```python
identity_matrix = make_matrix(5, 5, is_diagonal)
```


```bash
resultado: 
[[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 1]]
```