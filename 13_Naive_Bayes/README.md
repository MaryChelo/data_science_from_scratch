# Ingenuo bayes

## Filtro de Spam

Una red social no es muy buena si la gente no puede conectarse. En consecuencia, DataSciencester tiene una característica popular que permite a los miembros enviar mensajes a otros miembros. Y mientras que la mayoría de sus miembros son ciudadanos responsables que solo envían mensajes bien recibidos de “¿cómo te va?”, Algunos malhechores persistentemente envían spam a otros miembros sobre planes para enriquecerse, productos farmacéuticos sin receta y credenciales científicas con fines de lucro. 

Primero, creemos una función simple para convertir los mensajes en palabras distintas. Primero convertiremos cada mensaje a minúsculas; use re.findall () para extraer "palabras" que consisten en letras, números y apóstrofes; y finalmente use set () para obtener solo las distintas palabras:

```python
def tokenize(message):
    message = message.lower()                       # se convierte a minúsculas
    all_words = re.findall("[a-z0-9']+", message)   # extrae las palabras
    return set(all_words)                           # eliminar duplicados
```

Nuestra segunda función contará las palabras en un conjunto de mensajes de entrenamiento etiquetado. Le devolveremos un diccionario cuyas claves son palabras y cuyos valores son listas de dos elementos [spam_count, non_spam_count] correspondientes a la cantidad de veces que vimos esa palabra en los mensajes de spam y no spam:

```python
def count_words(training_set):
    """el conjunto de entrenamiento consiste en pares (mensaje, is_spam)"""
    counts = defaultdict(lambda: [0, 0])
    for message, is_spam in training_set:
        for word in tokenize(message):
            counts[word][0 if is_spam else 1] += 1
    return counts
```

Nuestro siguiente paso es convertir estos conteos en probabilidades estimadas utilizando el suavizado que describimos anteriormente. Nuestra función devolverá una lista de los trillizos que contienen cada palabra, la probabilidad de ver esa palabra en un mensaje de spam y la probabilidad de ver esa palabra en un mensaje no spam:

```python
def word_probabilities(counts, total_spams, total_non_spams, k=0.5):
    """convertir los word_counts en una lista de trillizos
     w, p (w | spam) y p (w | ~ spam)"""
    return [(w,
             (spam + k) / (total_spams + 2 * k),
             (non_spam + k) / (total_non_spams + 2 * k))
             for w, (spam, non_spam) in counts.items()]
```

La última parte es usar estas probabilidades de palabra (y nuestras suposiciones de Naive Bayes) para asignar probabilidades a los mensajes:

```python
def spam_probability(word_probs, message):
    message_words = tokenize(message)
    log_prob_if_spam = log_prob_if_not_spam = 0.0

    for word, prob_if_spam, prob_if_not_spam in word_probs:

        # para cada palabra en el mensaje,
        # agregar la probabilidad de registro de verlo
        if word in message_words:
            log_prob_if_spam += math.log(prob_if_spam)
            log_prob_if_not_spam += math.log(prob_if_not_spam)

        # por cada palabra que no está en el mensaje
        # agrega la probabilidad de registro de no verlo
        else:
            log_prob_if_spam += math.log(1.0 - prob_if_spam)
            log_prob_if_not_spam += math.log(1.0 - prob_if_not_spam)

    prob_if_spam = math.exp(log_prob_if_spam)
    prob_if_not_spam = math.exp(log_prob_if_not_spam)
    return prob_if_spam / (prob_if_spam + prob_if_not_spam)
```

Podemos poner todo esto junto en nuestro clasificador Naive Bayes:

```python
class NaiveBayesClassifier:

    def __init__(self, k=0.5):
        self.k = k
        self.word_probs = []

    def train(self, training_set):

        # contar mensajes de spam y no spam
        num_spams = len([is_spam
                         for message, is_spam in training_set
                         if is_spam])
        num_non_spams = len(training_set) - num_spams

        # correr datos de entrenamiento a través de nuestro "pipeline"
        word_counts = count_words(training_set)
        self.word_probs = word_probabilities(word_counts,
                                             num_spams,
                                             num_non_spams,
                                             self.k)

    def classify(self, message):
        return spam_probability(self.word_probs, message)
```