users = [
{ "id": 0, "name": "Hero" },
{ "id": 1, "name": "Dunn" },
{ "id": 2, "name": "Sue" },
{ "id": 3, "name": "Chi" },
{ "id": 4, "name": "Thor" },
{ "id": 5, "name": "Clive" },
{ "id": 6, "name": "Hicks" },
{ "id": 7, "name": "Devin" },
{ "id": 8, "name": "Kate" },
{ "id": 9, "name": "Klein" }
]

friendships = [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4),
(4, 5), (5, 6), (5, 7), (6, 8), (7, 8), (8, 9)]


for user in users:
    user["friends"] = []

for i, j in friendships:
    # esto funciona porque los usuarios [i] son los usuarios cuya ID es i
    users[i]["friends"].append(users[j]) # agregar i como amigo de j
    users[j]["friends"].append(users[i]) # agregar j como amigo de i

def number_of_friends(user):
    #cuantos amigos tiene _user_?
    return len(user["friends"]) # longitud de la lista de friend_ids

total_connections = sum(number_of_friends(user) for user in users)

print("Total de conexiones: ", total_connections)

num_users=len(users)
avg_connections = total_connections / num_users # 2.4
print("Por número de usuarios ", avg_connections)

# crear una lista (user_id, number_of_friends)
num_friends_by_id = [(user["id"], number_of_friends(user)) for user in users]

print("usuarios ordenados por número de amigos:")

print(sorted(num_friends_by_id,
                 key=lambda pair: pair[1],                       # Por número de amigos
                 reverse=True))                                  # de mayor a menor   