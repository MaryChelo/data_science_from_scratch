# Trabajando con datos

## Conectores claves

Los conectores claves representan la conexión dentro de una red de datos donde se expresa un dato y su conexión con el siguiente.

En el siguiente ejemplo se muestra una <code>lista</code> representado por un `dict` que contiene <code>id</code> y su <code>nombre</code>

```python
users = [
{ "id": 0, "name": "Hero" },
{ "id": 1, "name": "Dunn" },
{ "id": 2, "name": "Sue" },
{ "id": 3, "name": "Chi" },
{ "id": 4, "name": "Thor" },
{ "id": 5, "name": "Clive" },
{ "id": 6, "name": "Hicks" },
{ "id": 7, "name": "Devin" },
{ "id": 8, "name": "Kate" },
{ "id": 9, "name": "Klein" }
]
```

Otra forma de representar lista de pares de IDs:

```python
friendships = [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4),(4, 5), (5, 6), (5, 7), (6, 8), (7, 8), (8, 9)
```

Visualmente se muestra como una red DataSciencester: <br/>
![](imagenes/1_redDatos.png)

## Listas

Se puede recorrer una lista mediante un bucle <code>for</code> y se puede agregar elementos con <code>append</code>

```python
for user in users:
    user["friends"] = []

for i, j in friendships:
    # esto funciona porque los usuarios [i] son los usuarios cuya ID es i
    users[i]["friends"].append(users[j]) # agregar i como amigo de j
    users[j]["friends"].append(users[i]) # agregar j como amigo de i
```

Una vez que cada usuario contiene una lista de amigos, podemos hacer preguntas de nuestro gráfico como **¿cuál es el número promedio de conexiones?**

1. Primero encontramos el número total de conexiones, al resumir las longitudes de todas las listas de amigos

```python
def number_of_friends(user):
    #cuantos amigos tiene _user_?
    return len(user["friends"]) # longitud de la lista de friend_ids

total_connections = sum(number_of_friends(user) for user in users)
```

2. Dividir por el número de usuarios

```python
num_users=len(users)
avg_connections = total_connections / num_users # 2.4

print("Por número de usuarios ", avg_connections)
```

También es fácil encontrar a las personas más conectadas, son las personas que tienen el mayor número de amigos.
Como no hay muchos usuarios, podemos clasificarlos de **la mayoría de los amigos** a **menos amigos**:

```python
# crear una lista (user_id, number_of_friends)
num_friends_by_id = [(user["id"], number_of_friends(user)) for user in users]

print("usuarios ordenados por número de amigos:")

print(sorted(num_friends_by_id,
                 key=lambda pair: pair[1],                       # Por número de amigos
                 reverse=True))                                  # de mayor a menor   
```

**Más ejemplos de `Clave, valor`**

En el siguiente ejemplo se muestra la relación (user_id, interés):

```python
interests = [
(0, "Hadoop"), (0, "Big Data"), (0, "HBase"), (0, "Java"),
(0, "Spark"), (0, "Storm"), (0, "Cassandra"),
(1, "NoSQL"), (1, "MongoDB"), (1, "Cassandra"), (1, "HBase"),
(1, "Postgres"), (2, "Python"), (2, "scikit-learn"), (2, "scipy"),
(2, "numpy"), (2, "statsmodels"), (2, "pandas"), (3, "R"), (3, "Python"),
(3, "statistics"), (3, "regression"), (3, "probability"),
(4, "machine learning"), (4, "regression"), (4, "decision trees"),
(4, "libsvm"), (5, "Python"), (5, "R"), (5, "Java"), (5, "C++"),
(5, "Haskell"), (5, "programming languages"), (6, "statistics"),
(6, "probability"), (6, "mathematics"), (6, "theory"),
(7, "machine learning"), (7, "scikit-learn"), (7, "Mahout"),
(7, "neural networks"), (8, "neural networks"), (8, "deep learning"),
(8, "Big Data"), (8, "artificial intelligence"), (9, "Hadoop"),
(9, "Java"), (9, "MapReduce"), (9, "Big Data")
]
```

Es fácil construir una función que encuentre usuarios con cierto interés:

```python
def data_scientists_who_like(target_interest):
    return [user_id
            for user_id, user_interest in interests
            if user_interest == target_interest]

print(data_scientists_who_like("machine learning"))
```

Esto funciona, pero tiene que examinar la lista completa de intereses para cada búsqueda. Si tenemos muchos usuarios e intereses (o si solo queremos hacer muchas búsquedas), probablemente estemos mejor construyendo un índice de intereses a usuarios:

```python
from collections import defaultdict
#las claves son intereses, los valores son listas de user_ids con ese interés
user_ids_by_interest = defaultdict(list)
for user_id, interest in interests:
    user_ids_by_interest[interest].append(user_id)

print(user_ids_by_interest)
```

Y otra de usuarios a intereses:

```python
interests_by_user_id = defaultdict(list)
for user_id, interest in interests:
    interests_by_user_id[user_id].append(interest)
```

Ahora es fácil encontrar quién tiene más intereses en común con un usuario determinado:
1. Iterar sobre los intereses del usuario.
2. Para cada interés, repita sobre los otros usuarios con ese interés.
3. Mantenga la cuenta de cuántas veces nos vemos a otros usuarios.

```python
def most_common_interests_with(user_id):
    return Counter(interested_user_id
        for interest in interests_by_user_id["user_id"]
            for interested_user_id in user_ids_by_interest[interest]
                if interested_user_id != user_id)
```

Una forma sencilla de encontrar los intereses más populares es simplemente contar las palabras:
1. Minúsculas en cada interés (ya que diferentes usuarios pueden o no pueden capitalizar sus
intereses).
2. Divídelo en palabras.
3. Cuenta los resultados.

```python
words_and_counts = Counter(word 
                           for user, interest in interests 
                           for word in interest.lower().split())                    
```

Dando como resultado:

```bash
Counter({'big': 3, 'data': 3, 'java': 3, 'python': 3, 'learning': 3, 'hadoop': 2, 'hbase': 2, 'cassandra': 2, 'scikit-learn': 2, 'r': 2, 'statistics': 2, 'regression': 2, 'probability': 2, 'machine': 2, 'neural': 2, 'networks': 2, 'spark': 1, 'storm': 1, 'nosql': 1, 'mongodb': 1, 'postgres': 1, 'scipy': 1, 'numpy': 1, 'statsmodels': 1, 'pandas': 1, 'decision': 1, 'trees': 1, 'libsvm': 1, 'c++': 1, 'haskell': 1, 'programming': 1, 'languages': 1, 'mathematics': 1, 'theory': 1, 'mahout': 1, 'deep': 1, 'artificial': 1, 'intelligence': 1, 'mapreduce': 1})```

## Graficando datos

Se requiere importar la libreria <code>pyplot </code> 

```python
from matplotlib import pyplot as plt
```

Teniendo el ejemplo de Salarios y Experiencia

```python
salaries_and_tenures = [(83000, 8.7), (88000, 8.1),
                        (48000, 0.7), (76000, 6),
                        (69000, 6.5), (76000, 7.5),
                        (60000, 2.5), (83000, 10),
                        (48000, 1.9), (63000, 4.2)]
```

```python                    
def make_chart_salaries_by_tenure():
    tenures = [tenure for salary, tenure in salaries_and_tenures]
    salaries = [salary for salary, tenure in salaries_and_tenures]
    plt.scatter(tenures, salaries)
    plt.xlabel("Years Experience")
    plt.ylabel("Salary")
    plt.show()
make_chart_salaries_by_tenure()
```

![](imagenes/2_yearsExperience.png)
   