import math, random
from matplotlib import pyplot as plt
def inverse_normal_cdf(p, mu=0, sigma=1, tolerance=0.00001):
    """encontrar inverso aproximado utilizando la búsqueda binaria"""

    # si no es estándar, computar estándar y volver a escalar
    if mu != 0 or sigma != 1:
        return mu + sigma * inverse_normal_cdf(p, tolerance=tolerance)

    low_z, low_p = -10.0, 0            # normal_cdf (-10) es (muy cerca de) 0
    hi_z,  hi_p  =  10.0, 1            # normal_cdf (10) es (muy cerca de) 1
    while hi_z - low_z > tolerance:
        mid_z = (low_z + hi_z) / 2     # considera el punto medio
        mid_p = normal_cdf(mid_z)      # y el valor del cdf allí
        if mid_p < p:
            # el punto medio sigue siendo demasiado bajo, busque por encima
            low_z, low_p = mid_z, mid_p
        elif mid_p > p:
            # el punto medio sigue siendo demasiado alto, busque debajo de él
            hi_z, hi_p = mid_z, mid_p
        else:
            break

    return mid_z

def random_normal():
    """devuelve un sorteo aleatorio de una distribución normal estándar"""
    return inverse_normal_cdf(random.random())
xs = [random_normal() for _ in range(1000)]
ys1 = [ x + random_normal() / 2 for x in xs]
ys2 = [-x + random_normal() / 2 for x in xs]

print(random_normal())

plt.scatter(xs, ys1, marker='.', color='black', label='ys1')
plt.scatter(xs, ys2, marker='.', color='gray', label='ys2')
plt.xlabel('xs')
plt.ylabel('ys')
plt.legend(loc=9)
plt.title("Very Different Joint Distributions")
plt.show()
