#--Comparación de dos distribuciones--

from collections import Counter
import math, random
from matplotlib import pyplot as plt

def compare_two_distributions():

    random.seed(0)

    uniform = [random.randrange(-100,101) for _ in range(200)]
    normal = [57 * inverse_normal_cdf(random.random())
              for _ in range(200)]

    plot_histogram(uniform, 10, "Uniform Histogram")
    plot_histogram(normal, 10, "Normal Histogram")

#--Funciones de probabilidad--
def inverse_normal_cdf(p, mu=0, sigma=1, tolerance=0.00001):
    """encontrar inverso aproximado utilizando la búsqueda binaria"""

    # si no es estándar, computar estándar y volver a escalar
    if mu != 0 or sigma != 1:
        return mu + sigma * inverse_normal_cdf(p, tolerance=tolerance)

    low_z, low_p = -10.0, 0            # normal_cdf (-10) es (muy cerca de) 0
    hi_z,  hi_p  =  10.0, 1            # normal_cdf (10) es (muy cerca de) 1
    while hi_z - low_z > tolerance:
        mid_z = (low_z + hi_z) / 2     # considera el punto medio
        mid_p = normal_cdf(mid_z)      # y el valor del cdf allí
        if mid_p < p:
            # el punto medio sigue siendo demasiado bajo, busque por encima
            low_z, low_p = mid_z, mid_p
        elif mid_p > p:
            # el punto medio sigue siendo demasiado alto, busque debajo de él
            hi_z, hi_p = mid_z, mid_p
        else:
            break

    return mid_z

def normal_cdf(x, mu=0,sigma=1):
    return (1 + math.erf((x - mu) / math.sqrt(2) / sigma)) / 2

#--Mandar llamar la función---
compare_two_distributions()