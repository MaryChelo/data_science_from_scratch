# Trabajando con Datos

## Explorando sus datos

Después de identificar las preguntas que está tratando de responder y de tener en sus manos algunos datos, puede verse tentado a sumergirse e inmediatamente comenzar a construir modelos y obtener respuestas.

**Explorando datos unidimensionales**

<br/>

El caso más simple es cuando tiene un conjunto de datos unidimensionales, que es solo una colección de números. 
Por ejemplo:
* Cantidad promedio diaria de minutos que cada usuario pasa en su sitio.
* Cantidad de veces que se ha visto cada uno de una colección de videos de tutoría de ciencia de datos.
* Cantidad de páginas de cada uno de los libros de ciencia de datos en sus datos biblioteca de ciencias.

**Comparando dos distribuciones**

```python
def compare_two_distributions():

    random.seed(0)

    uniform = [random.randrange(-100,101) for _ in range(200)]
    normal = [57 * inverse_normal_cdf(random.random())
              for _ in range(200)]

    plot_histogram(uniform, 10, "Uniform Histogram")
    plot_histogram(normal, 10, "Normal Histogram")
```

![](imagenes/1_histogramaUniforme.png)

![](imagenes/2_histogramaNormal.png)

**Dos dimensiones**

Ahora imagine que tiene un conjunto de datos con dos dimensiones. Tal vez además de los minutos diarios tenga años de experiencia en ciencia de datos. Por supuesto que querrás entender cada dimensión individualmente. Pero probablemente también quieras dispersar los datos. Por ejemplo, considere otro conjunto de datos falso

```python
def random_normal():
    """devuelve un sorteo aleatorio de una distribución normal estándar"""
    return inverse_normal_cdf(random.random())
xs = [random_normal() for _ in range(1000)]
ys1 = [ x + random_normal() / 2 for x in xs]
ys2 = [-x + random_normal() / 2 for x in xs]

plt.scatter(xs, ys1, marker='.', color='black', label='ys1')
plt.scatter(xs, ys2, marker='.', color='gray', label='ys2')
plt.xlabel('xs')
plt.ylabel('ys')
plt.legend(loc=9)
plt.title("Very Different Joint Distributions")
plt.show()
```

![](imagenes/3_dosDimensiones.png)


**Multiples dimensiones**

Con muchas dimensiones, le gustaría saber cómo se relacionan entre sí todas las dimensiones. Un enfoque simple es observar la matriz de correlación.

```python
def correlation_matrix(data):
    """devuelve la matriz num_columns x num_columns cuya (i, j) th entrada
     Es la correlación entre las columnas i y j de datos."""

    _, num_columns = shape(data)

    def matrix_entry(i, j):
        return correlation(get_column(data, i), get_column(data, j))

    return make_matrix(num_columns, num_columns, matrix_entry)
```

Para graficarlos

```python
def make_scatterplot_matrix():
    
    # Primero, genera algunos datos aleatorios.

    num_points = 100

    def random_row():
        row = [None, None, None, None]
        row[0] = random_normal()
        row[1] = -5 * row[0] + random_normal()
        row[2] = row[0] + row[1] + 5 * random_normal()
        row[3] = 6 if row[2] > -2 else 0
        return row
    random.seed(0)
    data = [random_row()
            for _ in range(num_points)]
    _, num_columns = shape(data)
    fig, ax = plt.subplots(num_columns, num_columns)

    for i in range(num_columns):
        for j in range(num_columns):

            # dispersar column_j en el eje x vs column_i en el eje y
            if i != j: ax[i][j].scatter(get_column(data, j), get_column(data, i))

            # sino que muestre i == j, en cuyo caso muestre el nombre de la serie
            else: ax[i][j].annotate("series " + str(i), (0.5, 0.5),
                                    xycoords='axes fraction',
                                    ha="center", va="center")

            # luego oculte las etiquetas de los ejes, excepto los gráficos de la izquierda y la parte inferior
            if i < num_columns - 1: ax[i][j].xaxis.set_visible(False)
            if j > 0: ax[i][j].yaxis.set_visible(False)

    # corregir las etiquetas de los ejes inferior derecho e izquierdo, que están mal porque
    # sus gráficos solo tienen texto en ellos
    ax[-1][-1].set_xlim(ax[0][-1].get_xlim())
    ax[0][0].set_ylim(ax[0][1].get_ylim())

    plt.show()
```

![](imagenes/4_scatterplot_matrix.png)

Al observar los diagramas de dispersión, puede ver que la serie 1 está muy correlacionada negativamente con la serie 0, la serie 2 está correlacionada positivamente con la serie 1, y la serie 3 solo toma los valores 0 y 6, donde 0 corresponde a los valores pequeños de la serie 2 y 6 correspondientes a grandes valores.