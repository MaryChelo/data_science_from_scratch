# Machine Learning

##  Modelo

**¿Qué es un modelo?**

Es simplemente una especificación de una relación matemática (o probabilística) que existe entre diferentes variables. 
Por ejemplo, si está tratando de recaudar dinero para su sitio de redes sociales, podría crear un modelo de negocio (probablemente en una hoja de cálculo) que tome entradas como "número de usuarios" e "ingresos de anuncios por usuario" y "número de empleados ”Y produce su ganancia anual para los próximos años. 

**¿cómo nos aseguramos de que nuestros modelos no sean demasiado complejos?**

El enfoque más fundamental implica el uso de diferentes datos para entrenar el modelo y probar el modelo. 

La forma más sencilla de hacerlo es dividir su conjunto de datos, de modo que (por ejemplo) dos tercios de este se usen para entrenar el modelo, después de lo cual medimos el rendimiento del modelo en el tercio restante:

```python
from collections import Counter
import math, random

def split_data(data, prob):
    """divide los datos en fracciones [prob, 1 - prob]"""
    results = [], []
    for row in data:
        results[0 if random.random() < prob else 1].append(row)
    return results
```

A menudo, tendremos una matriz x de variables de entrada y un vector y de variables de salida. En ese caso, debemos asegurarnos de reunir los valores correspondientes en los datos de entrenamiento o en los datos de prueba:

```python
def train_test_split(x, y, test_pct):
    data = list(zip(x, y))                        # par valores correspondientes
    train, test = split_data(data, 1 - test_pct)  # divide el conjunto de datos de pares
    x_train, y_train = list(zip(*train))          # truco un-zip 
    x_test, y_test = list(zip(*test))
    return x_train, x_test, y_train, y_test
```

Luego podemos usarlos para calcular varias estadísticas sobre el rendimiento del modelo. 

* <code>Accuracy</code> se define como la fracción de predicciones correctas:

```python
def accuracy(tp, fp, fn, tn):
    correct = tp + tn
    total = tp + fp + fn + tn
    return correct / total
```

* <code>Precisión</code> mide la precisión de nuestras predicciones positivas:

```python
def precision(tp, fp, fn, tn):
    return tp / (tp + fp)
```

* <code>Recall</code> mide qué fracción de los aspectos positivos que nuestro modelo identificó:

```python
def recall(tp, fp, fn, tn):
    return tp / (tp + fn)
```

A veces, la precisión y el recuerdo se combinan en <code>f1_score</code>, que se define como:

```python
def f1_score(tp, fp, fn, tn):
    p = precision(tp, fp, fn, tn)
    r = recall(tp, fp, fn, tn)

    return 2 * p * r / (p + r)
```

Al ejecutarlo:

```python
if __name__ == "__main__":

    print("accuracy(70, 4930, 13930, 981070)", accuracy(70, 4930, 13930, 981070))
    print("precision(70, 4930, 13930, 981070)", precision(70, 4930, 13930, 981070))
    print("recall(70, 4930, 13930, 981070)", recall(70, 4930, 13930, 981070))
    print("f1_score(70, 4930, 13930, 981070)", f1_score(70, 4930, 13930, 981070))
```

Resultado:

```
accuracy(70, 4930, 13930, 981070) 0.98114
precision(70, 4930, 13930, 981070) 0.014
recall(70, 4930, 13930, 981070) 0.005
f1_score(70, 4930, 13930, 981070) 0.00736842105263158
```